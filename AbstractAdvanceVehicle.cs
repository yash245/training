﻿using System;

namespace Training
{
    abstract class Vehicle
    {
        private string _vehicleName; //defination of variables
         /// <summary>
         /// getter setter method for vehicle name
         /// </summary>
        public string VehicleName 
        {
            get
            {
                return _vehicleName;
            }
            set
            {
                this._vehicleName = value;
            }

        }
          /// <summary>
          /// getter setter method for vehicle color
          /// </summary>
        string _vehicleColor;   //defination of variables
        public string VehicleColor
        {
            get
            {
                return _vehicleColor;
            }
            set
            {
                this._vehicleColor = value;
            }

        }
        public int _numberOfWheels, _speedLimit; //defination of variables
        const int maxSpeed = 60;//creating a constant variable 
        public virtual void VehicleWheels(string vehicleName) //Initialization of number of wheels in vehicles 
        {
            this._vehicleName = vehicleName;
            if (_vehicleName == "bike")
                Console.WriteLine("numberOfWheels =" + 2);
            else if (_vehicleName == "auto")
                Console.WriteLine("numberOfWheels =" + 3);
            else if (_vehicleName == "car")
                Console.WriteLine("numberOfWheels =" + 4);
            else
                Console.WriteLine("numberOfWheels =" + 6);

        }
        public void Start() //vehicle going to be started
        {
            Console.WriteLine("Start>>>>>>");
        }
        public void Stop() //vehicle to be stopped
        {
            Console.WriteLine("Stop!!!!");

        }
        public abstract void SpeedUp(int recentSpeed);  //declaration os a abstract member
        public abstract void CalcTollAmount();   //declaration os a abstract member
    }


    class Car : Vehicle  //inheriting the vehicle class 
    {
        public readonly int maxSpeed; //readonly variable for maxspeed
        int tollCharge = 40;   //initiallizing of toll charge
        public Car(string carColor, int newMaxSpeed) : base()
        {
            Console.WriteLine("your vehicle color is " + carColor);  //accesssing the parents values
            this.maxSpeed = newMaxSpeed;

        }
        public override void CalcTollAmount() // overriding of a abstract calculate the toll charge member
        {
            Console.WriteLine("your toll charges is = " + tollCharge +" rupee");
        }
          /// <summary>
          /// calculating the speed limit of a car 
          /// </summary>
          /// <param name="recentSpeed">current speed of a car</param>
        public override void SpeedUp(int recentSpeed)    //checking speed override to the vehicle class
        {
            if (recentSpeed >= maxSpeed)
            {
                Console.WriteLine("over speeding plz slow down!!");
                Console.WriteLine("your speed is up by " + (recentSpeed - maxSpeed) + " Km/hr");
            }
            else
                Console.WriteLine("you are under speedlimit no vary!!");
        }

    }
    class Bike : Vehicle //inheriting the vehicle class 
    {
        public readonly int maxSpeed;  //readonly variable for maxspeed
        int tollCharge = 20; //initiallizing of toll charge

        public Bike(String newcolor, int newMaxSpeed) : base()
        {
            Console.WriteLine("your vehicle color is " + newcolor);  //accesssing the parents values
            this.maxSpeed = newMaxSpeed;

        }
        public override void CalcTollAmount() // overriding of a abstract calculate the toll charge member
        {
            Console.WriteLine("your toll charges is = " + tollCharge + " rupee"); 
        }
        /// <summary>
        /// calculating the speed limit of a bike  
        /// </summary>
        /// <param name="recentSpeed">current speed of a car</param>
        public override void SpeedUp(int recentSpeed)    //checking speed override to the vehicle class
        {
            if (recentSpeed >= maxSpeed)
            {
                Console.WriteLine("over speeding plz slow down!!");
                Console.WriteLine("your speed is up by " + (recentSpeed - maxSpeed) + " Km/hr");
            }
            else
                Console.WriteLine("you are under speedlimit no vary!!");
        }
    }
    public class AbstractAdvanceVehicle
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the name of vehicle"); //reading the name of a vehicle
            string name = Console.ReadLine();

            if (name == "bike")  //if the client is selecting information about a bike
            {
                Console.WriteLine("Enter your new maxspeed");
                string checkSpeed = Console.ReadLine();

                int speed;
                if (Int32.TryParse(checkSpeed, out speed)) //checking integer or not
                    Console.WriteLine("valid speed");
                else
                    Console.WriteLine("wrong!!! you entered not a vaild speed");

                Console.WriteLine("Enter your car color");
                string newColor = Console.ReadLine();
                Vehicle vehicleObject = new Bike(newColor, speed); //Creating an object of the vehicle class
                vehicleObject.Start();
                vehicleObject.VehicleName = name;
                vehicleObject.VehicleColor = newColor;
                vehicleObject.VehicleWheels(name);
                Bike bikeObj = new Bike(newColor, speed); //Creating an object of the bike class
                bikeObj.CalcTollAmount();
                Console.WriteLine("Enter your current speed"); //reading the current speed of a vehicle
                string recentSpeed = Console.ReadLine();
                int currentSpeed;
                if (Int32.TryParse(recentSpeed, out currentSpeed)) //checking integer or not
                    Console.WriteLine("you enter a valid speed");
                else
                    Console.WriteLine("wrong!!! you have entered not a vaild speed");
                bikeObj.SpeedUp(currentSpeed);
                vehicleObject.Stop();
            }
            else if (name == "car") //if the client is selecting information about a car
            {
                Console.WriteLine("Enter your new maxspeed");
                string checkSpeed = Console.ReadLine();

                int speed;
                if (Int32.TryParse(checkSpeed, out speed))  //checking integer or not
                    Console.WriteLine("you enter a valid speed");
                else
                    Console.WriteLine("wrong!!! you have entered not a vaild speed");

                Console.WriteLine("Enter your car color");
                string newColor = Console.ReadLine();
                Vehicle vehicleObject = new Car(newColor, speed); //Creating an object of the vehicle class
                vehicleObject.Start();
                vehicleObject.VehicleName = name;
                vehicleObject.VehicleColor = newColor;
                vehicleObject.VehicleWheels(name);
                Car carObj = new Car(newColor, speed); //Creating an object of the car class
                carObj.CalcTollAmount(); //calling function
                Console.WriteLine("Enter your current speed"); //reading the current speed of a vehicle
                string recentSpeed = Console.ReadLine();
                int currentSpeed;
                if (Int32.TryParse(recentSpeed, out currentSpeed))  //checking integer or not
                    Console.WriteLine("you enter a valid speed");
                else
                    Console.WriteLine("wrong!!! you have entered not a vaild speed");
                carObj.SpeedUp(currentSpeed);
                vehicleObject.Stop();
            }
            else
                Console.WriteLine("you have enter a wrong vehicle");

            Console.ReadKey();

        }
    }
}
