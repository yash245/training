﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EmployDataAccess;

namespace EmployeeService.Controllers
{
    public class EmployeeController : ApiController
    {
        public IEnumerable<EmployeeDetail> Get()
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                return entity.EmployeeDetails.ToList();
            }
        }
        public HttpResponseMessage Get(int id)
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                EmployeeDetail msg = entity.EmployeeDetails.FirstOrDefault(e => e.empId == id);
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, msg);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, id.ToString() + "  Not Found in the employee Table");
                }
            }
        }


        public HttpResponseMessage Post([FromBody] EmployeeDetail employee)
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                try
                {
                    entity.EmployeeDetails.Add(employee);
                    entity.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, employee);
                    message.Headers.Location = new Uri(Request.RequestUri + employee.empId.ToString());
                    return message;
                }
                catch (Exception)
                { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Please enter the data with an another id"); }
            }
        }

        public string Delete(int id)
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                int count = entity.EmployeeDetails.Where(e => e.empId == id).Count();
                if (count >0)
                {
                    EmployeeDetail detail = entity.EmployeeDetails.FirstOrDefault(e => e.empId == id);
                    entity.EmployeeDetails.Remove(detail);
                    entity.SaveChanges();
                    return "Record Deleted successfully";
                }
                else
                {
                    count = 0;
                    return "Record Not Found";
                }
            }

        }
    }
}

