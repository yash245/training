﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;

namespace EmployeeService.Controllers
{
    public partial class Employee
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public int Age { get; set; }
    }
    public class SqlController : ApiController
    {
        public IEnumerable<Employee> Get()
        {
            SqlConnection con = new SqlConnection("data Source =LAPR253;initial catalog=employee;integrated security=false;user id=sa;password =lala@123");
            con.Open();
                SqlCommand selectTable = new SqlCommand("select * from EmployeeDetails ;", con);
                SqlDataReader reader = selectTable.ExecuteReader();

                List<Employee> employee = new List<Employee>();
                while (reader.Read())
                {
                    employee.Add(new Employee { EmpId = (int)reader["empId"], EmpName = reader["empName"].ToString(), Age = (int)reader["age"] });
                }
                con.Close();
            return employee;
           
        }
        public HttpResponseMessage Get(int id)
        {
            try
            {
                SqlConnection con = new SqlConnection("data Source =LAPR253;initial catalog=employee;integrated security=false;user id=sa;password =lala@123");
                con.Open();
                SqlCommand selectTable = new SqlCommand("select * from EmployeeDetails where empId = "+id+" ;", con);
                SqlDataReader reader = selectTable.ExecuteReader();

                List<Employee> employee = new List<Employee>();
                while (reader.Read())
                {
                    employee.Add(new Employee { EmpId = (int)reader["empId"], EmpName = reader["empName"].ToString(), Age = (int)reader["age"] });
                }
                con.Close();
                if (employee != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, employee);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "  Not Found in the employee Table");
                }
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Sql Exception from get");
            }

        }
        public HttpResponseMessage Post([FromBody]Employee emp)
        {
            try
            {
                if (emp != null)
                {
                    SqlConnection con = new SqlConnection("data Source =LAPR253;initial catalog=employee;integrated security=false;user id=sa;password =lala@123");
                    con.Open();
                    SqlCommand selectTable = new SqlCommand("insert into EmployeeDetails values {" + emp.EmpId + ", '" + emp.EmpName + "' ," + emp.Age + " };", con);
                    selectTable.ExecuteNonQuery();
                    List<Employee> employee = new List<Employee>();
                    con.Close();
                    return Request.CreateResponse(HttpStatusCode.OK, employee);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, " You doesn't give any input in the request body");
                }
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Sql Exception because you have entered an invalid synatx");
            }

        }
        public HttpResponseMessage Put(int id, [FromBody]Employee emp)
        {
            try
            {
                SqlConnection con = new SqlConnection("data Source =LAPR253;initial catalog=employee;integrated security=false;user id=sa;password =lala@123");
                con.Open();
                SqlCommand selectTable = new SqlCommand("select * from EmployeeDetails where empId = " + id, con);
                SqlDataReader reader = selectTable.ExecuteReader();
                List<Employee> employee = new List<Employee>();
                while (reader.Read())
                {
                    employee.Add(new Employee { EmpId = (int)reader["empId"], EmpName = reader["empName"].ToString(), Age = (int)reader["age"] });
                }
                if (employee != null)
                {
                    SqlCommand select = new SqlCommand("update EmployeeDetails set empName ='" + emp.EmpName + "' , age = " + emp.Age + " where empId= " + id, con);
                    select.ExecuteReader();
                    con.Close();
                    return Request.CreateResponse(HttpStatusCode.OK, employee);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "  Not Found in the employee Table");
                }
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Sql Exception");
            }

        }
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (id != 0)
                {
                    SqlConnection con = new SqlConnection("data Source =LAPR253;initial catalog=employee;integrated security=false;user id=sa;password =lala@123");
                    con.Open();
                    SqlCommand selectTable = new SqlCommand("delete from EmployeeDetails where empId =" + id, con);
                    selectTable.ExecuteNonQuery();
                    List<Employee> employee = new List<Employee>();
                    con.Close();
                    return Request.CreateResponse(HttpStatusCode.OK, employee);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, " You doesn't give any input in the uri");
                }
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Sql Exception because you have entered an invalid synatx");
            }

        }

    }
}
