﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training
{
    class StringIsRefType
    {
        static void Main(string[] args)
        {
            string stringValue = "my name is lakha";
            string stringRefValue = stringValue;
            Console.WriteLine("printing the string value at str1" + stringValue);
            Console.WriteLine("now the string value(str2) is printed");
            Console.WriteLine(stringRefValue);
            Console.WriteLine("now i am changing the value of str1 to abc");
            stringValue = "abc";
            Console.WriteLine("printing the string value at str1" + stringValue);
            Console.WriteLine("printing the string value at str2" + stringRefValue);
            Console.WriteLine("It still poting to the first string that shows it is a refrence");
            Console.ReadKey();

        }
    }
}





