﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training
{
    class FindMonths
    {
        static void Main(string[] args)
        {
            char monthToFound = 'J';
            string[] months = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            string month;
            for (int index = 0; index < months.Length; index++)
            {
                month = months[index];
                if (month[0] == monthToFound)
                    Console.WriteLine(month);
            }
            Console.ReadKey();
        }
    }
}
