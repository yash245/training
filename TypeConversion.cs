﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training
{
    class TypeConversion
    {
        static void Main(string[] args)
        {
            char charValue = 'a';
            int charToInt = charValue;
            long charToLong = charValue;
            long intToLong = charToInt;
            float longToFloat = charToLong;
            float charToFloat = charValue;
            double charToDouble = charToFloat;
            Console.WriteLine("Type conversion");
            Console.WriteLine("Implicit type conversions");  //Implicit TypeConversion
            Console.WriteLine("Value to char is " + charValue);
            Console.WriteLine("char value to int is" + charToInt);
            Console.WriteLine("char to long is" + charToLong);
            Console.WriteLine("long to float is" + longToFloat);
            Console.WriteLine("float to double is" + charToDouble);
            Console.WriteLine("int to long is" + intToLong);
            Console.WriteLine("char to float is" + charToFloat);

            Console.WriteLine("\n\texplicit type conversions"); //Explicit TypeConversion

            double doubleValue = 263177.56523;
            float doubleToFloat = (float)doubleValue;
            long doubleToLong = (long)doubleValue;
            int do_int = (int)doubleValue;
            Console.WriteLine("double=" + doubleValue);
            Console.WriteLine("float=" + doubleToFloat);
            Console.WriteLine("long=" + doubleToLong);
            Console.WriteLine("int=" + do_int);

            Console.WriteLine("\nother conversions");
            Console.WriteLine("double to int 32=" + Convert.ToInt32(doubleValue));
            Console.WriteLine("float to uint32=" + Convert.ToUInt32(doubleToFloat));
            Console.WriteLine("Flaot to string=" + Convert.ToString(doubleToFloat));
            Console.WriteLine("int to double= " + Convert.ToDouble(do_int));
            
            Console.ReadKey();
        }
    }
}
