﻿using System;

namespace Training1
{
    public class Vehicle
    {
        private string _vehicleName;   //defination of variables
                                     /// <summary>
                                     /// getter setter method for vehicle name 
                                     /// </summary>
        public string VehicleName
        {
            get
            {
                return _vehicleName;
            }
            set
            {
                this._vehicleName = value;
            }

        }
        string _vehicleColor;   //defination of variables
        public int numberOfWheels, speedLimit; //defination of variables
        const int maxSpeed = 60;//creating a constant variable 
        public Vehicle(string vehicleName) //Initialization of number of wheels in vehicles 
        {
            this._vehicleName = vehicleName;
            if (_vehicleName == "bike")
                Console.WriteLine("numberOfWheels =" + 2);
            else if (_vehicleName == "auto")
                Console.WriteLine("numberOfWheels =" + 3);
            else if (_vehicleName == "car")
                Console.WriteLine("numberOfWheels =" + 4);
            else
                Console.WriteLine("numberOfWheels =" + 6);

        }
        public void Start() //vehicle going to be started
        {
            Console.WriteLine("Start>>>>>>");
        }
        public void Stop() //vehicle to be stopped
        {
            Console.WriteLine("Stop!!!!");

        }
        public void SpeedUp(int recentSpeed)    //checking speed 
        {
            if (recentSpeed >= maxSpeed)
            {
                Console.WriteLine("over speeding plz slow down!!");
                Console.WriteLine("your speed is up by " + (recentSpeed - maxSpeed) + " Km/hr");
            }
            else
                Console.WriteLine("you are under speedlimit no vary!!");
        }
    }


    public class Car : Vehicle  //inheriting the vehicle class 
    {
        public readonly int maxSpeed;
        int tollCharge = 40;
        public Car(String newcolor, int numWheels, int newMaxSpeed) : base("car")
        {
            base.color = newcolor;  //accesssing the parents values
            base.numberOfWheels = numWheels;  //accesssing the parents values
            this.maxSpeed = newMaxSpeed;

        }
        public void CalcTollAmount() // calculate the toll charge
        {
            Console.WriteLine("your toll charges is" + tollCharge);
        }
        public void SpeedUp(int recentSpeed)    //checking speed override to the vehicle class
        {
            if (recentSpeed >= maxSpeed)
            {
                Console.WriteLine("over speeding plz slow down!!");
                Console.WriteLine("your speed is up by " + (recentSpeed - maxSpeed) + " Km/hr");
            }
            else
                Console.WriteLine("you are under speedlimit no vary!!");
        }

    }
    class Bike : Vehicle //inheriting the vehicle class 
    {
        public readonly int maxSpeed;
        int tollCharge = 20;

        public Bike(String newcolor, int numWheels, int newMaxSpeed) : base("bike")
        {
            base.color = newcolor;  //accesssing the parents values
            base.numberOfWheels = numWheels;  //accesssing the parents values
            this.maxSpeed = newMaxSpeed;

        }
        public void CalcTollAmount() // calculate the toll charge
        {
            Console.WriteLine("your toll charges is" + tollCharge);
        }
        public void SpeedUp(int recentSpeed)    //checking speed override to the vehicle class
        {
            if (recentSpeed >= maxSpeed)
            {
                Console.WriteLine("over speeding plz slow down!!");
                Console.WriteLine("your speed is up by " + (recentSpeed - maxSpeed) + " Km/hr");
            }
            else
                Console.WriteLine("you are under speedlimit no vary!!");
        }
    }
    public class AdvanceVehicle
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the name of vehicle"); //reading the name of a vehicle
            string name = Console.ReadLine();
            if (name == "bike")
            {
                Vehicle vehicleObject = new Vehicle(name); //Creating an object of the vehicle class
                vehicleObject.Start();
                vehicleObject.VehicleName = name;
                Console.WriteLine("Enter your new maxspeed");
                string checkSpeed = Console.ReadLine();

                int speed;
                if (Int32.TryParse(checkSpeed, out speed))  //checking integer or not
                    Console.WriteLine("you enter a valid speed");
                else
                    Console.WriteLine("wrong!!! you have entered not a vaild speed");

                Console.WriteLine("Enter your car color");
                string newColor = Console.ReadLine();
                Bike bikeObj = new Bike(newColor, 2, speed); //Creating an object of the bike class
                bikeObj.CalcTollAmount();
                Console.WriteLine("Enter your current speed"); //reading the current speed of a vehicle
                string recentSpeed = Console.ReadLine();
                int currentSpeed;
                if (Int32.TryParse(recentSpeed, out currentSpeed))  //checking integer or not
                    Console.WriteLine("you enter a valid speed");
                else
                    Console.WriteLine("wrong!!! you have entered not a vaild speed");

                bikeObj.SpeedUp(currentSpeed);
                vehicleObject.Stop();
            }
            if (name == "car")
            {
                Vehicle vehicleObject = new Vehicle(name); //Creating an object of the vehicle class
                vehicleObject.Start();
                vehicleObject.VehicleName = name;
                Console.WriteLine("Enter your new maxspeed");
                string checkSpeed = Console.ReadLine();

                int speed;
                if (Int32.TryParse(checkSpeed, out speed))  //checking integer or not
                    Console.WriteLine("you enter a valid speed");
                else
                    Console.WriteLine("wrong!!! you have entered not a vaild speed");


                Console.WriteLine("Enter your car color");
                string newColor = Console.ReadLine();
                Car carObj = new Car(newColor, 2, speed); //Creating an object of the car class
                carObj.CalcTollAmount(); //calling function
                Console.WriteLine("Enter your current speed"); //reading the current speed of a vehicle
                string recentSpeed = Console.ReadLine();
                int currentSpeed;
                if (Int32.TryParse(recentSpeed, out currentSpeed))  //checking integer or not
                    Console.WriteLine("you enter a valid speed");
                else
                    Console.WriteLine("wrong!!! you have entered not a vaild speed");

                carObj.SpeedUp(currentSpeed);
                vehicleObject.Stop();
            }
            Console.ReadKey();
        }
    }
}
