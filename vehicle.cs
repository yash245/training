﻿using System;

namespace Training
{
    public class Vehicle
    {
        private string _vehicleName; //defination of variables
        public string VehicleName
        {
            get
            {
                return _vehicleName;
            }
            set
            {
                this._vehicleName = value;
            }

        }
        string _vehicleColor;   //defination of variables
        public string VehicleColor
        {
            get
            {
                return _vehicleColor;
            }
            set
            {
                this._vehicleColor = value;
            }

        }
        int _numberOfWheels, _speedLimit; //defination of variables
        public const int MaxSpeed = 60;//creating a constant variable 
        Vehicle(string vehicleName) //Initialization of number of wheels in vehicles 
        {
            this._vehicleName = vehicleName;
            if (vehicleName == "bike")
                Console.WriteLine("numberOfWheels =" + 2);
            else if (vehicleName == "auto")
                Console.WriteLine("numberOfWheels =" + 3);
            else if (vehicleName == "car")
                Console.WriteLine("numberOfWheels =" + 4);
            else
                Console.WriteLine("numberOfWheels =" + 6);

        }
        void Start() //vehicle going to be started
        {
            Console.WriteLine("Start>>>>>>");
        }
        void Stop() //vehicle to be stopped
        {
            Console.WriteLine("Stop!!!!");

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="recentSpeed">enter the current speed</param>
        void SpeedUp(int recentSpeed)    //checking speed 
        {
            if (recentSpeed > MaxSpeed)
            {
                Console.WriteLine("over speeding plz slow down!!");
                Console.WriteLine("your speed is up by " + (recentSpeed - MaxSpeed) + " Km/hr");
            }
            else
                Console.WriteLine("you are under speedlimit no vary!!");
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter the name of vehicle"); //reading the name of a vehicle
            string name = Console.ReadLine();
            Vehicle vehicleObject = new Vehicle(name); //Creating an object of the vehicle class
            vehicleObject.Start();
            Console.WriteLine("Enter your current speed"); //reading the current speed of a vehicle
            int currentSpeed = Console.Read();
            vehicleObject.SpeedUp(currentSpeed);
            vehicleObject.Stop();
            Console.ReadKey();
        }
    
    }
}