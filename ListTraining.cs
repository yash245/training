﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;


namespace Training
{
    class ListTraining
    {
        public static void main()
        {
            List<string> fruitList = new List<string>;
            fruitList.Add("apple");
            fruitList.Add("banana");
            fruitList.Add("cardom");
            fruitList.Add("dates");
            foreach (string fruit in fruitList) //printing the list
            {
                Console.WriteLine(fruit);

            }
            fruitList.Remove("apple"); //adding the element in the list
            foreach (string fruit in fruitList) 
            {
                Console.WriteLine(fruit);

            }
            fruitList.RemoveAt(3); //removing element at index of 3
            foreach (string fruit in fruitList)
            {
                Console.WriteLine(fruit);

            }
            fruitList.Insert(0,"alponso"); //inserting element at index of 0
            foreach (string fruit in fruitList)
            {
                Console.WriteLine(fruit);

            }
          
            string[] a = new string[fruitList.Count]; //initialising the index of the array
            fruitList.CopyTo(a); //coping the element of list in the array

            IList<string> fruits = new List<string>; // forming a ilist 
            fruits.Add("mango");
            fruits.Add("grapes");
            fruits.Add("almond");
            Console.WriteLine("finding the index of the grapes in list" + fruits.IndexOf("grapes")); //finding the index of grapes
            foreach (string fruit in fruits)
            {
                Console.WriteLine(fruit);

            }
            Console.WriteLine(fruits.Contains("almond")); //checking is the list ciontains almond or not
            fruits.Clear(); //clearing the elements of the list
            Console.WriteLine("after clearing elements length of fruit list" + fruits.Count);

            ObservableCollection<string> observeList = new ObservableCollection<string>;
            observeList.Add("leathershoes");
            observeList.Add("casualshoes");
            observeList.Add("formalshoes");
            foreach (string shoes in observeList)
            {
                Console.WriteLine(shoes);

            }

            ObservableCollection<string> observableList = observeList ; //giving the reference of the observablelist
            observableList.Add("batashoes");  //adding the element using the refrence of the observable list
            foreach (string shoes in observeList)
            {
                Console.WriteLine(shoes);

            }
            observableList.Remove("leathershoes"); //removing leathershoes in the list
            foreach (string shoes in observeList)
            {
                Console.WriteLine(shoes);

            }
            observableList.RemoveAt(1); //removing at the index element of the list
            foreach (string shoes in observeList)
            {
                Console.WriteLine(shoes);

            }










        }
    }
}
