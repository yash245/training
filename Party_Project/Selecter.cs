﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Party_Project
{
    class Selecter
    {
        public void SelectEvent(string venuePrice, float packageprice)
        {

            string retakes = null;
            Console.WriteLine("<<<< There are some selected events we have organise>>>>>\n" + "1.Birthday Party\n" + "2.Marriage ceremony\n" + "3.Anniversery ceremony");
            Console.WriteLine("Enter your input in the numbers 1,2 & 3");
            int selEvent;
            Console.WriteLine("\n");
            do
            {
                if (Int32.TryParse(Console.ReadLine(), out selEvent)) //checking integer or not
                {
                    string retake = null;

                    if (selEvent == 1)
                    {
                        Birthday birthObj = new Birthday();
                        birthObj.CalExpenditure(venuePrice, packageprice);
                        birthObj.CalPersonExpenditure();
                    }
                    else if (selEvent == 3)
                    {
                        AnniversaryCeremony annObj = new AnniversaryCeremony();
                        annObj.CalExpenditure(venuePrice, packageprice);
                        annObj.CalPersonExpenditure();
                    }
                    else if (selEvent == 2)
                    {
                        MarriageCeremony marriObj = new MarriageCeremony();
                        marriObj.CalExpenditure(venuePrice, packageprice);
                        marriObj.CalPersonExpenditure();
                    }
                    else
                    {
                        Console.WriteLine("WRONG!!! you entered an invalid");
                        Console.WriteLine("Please! Enter a correct input");
                        SelectEvent(venuePrice, packageprice);
                    }
                }

                else
                {
                    Console.WriteLine("WRONG!!! you entered an invalid choice");
                    Console.WriteLine("Do you want to CONTINUE!! Please your suggestion in the y for Yes ");
                    retakes = Console.ReadLine();
                }

            } while (retakes == "y" || retakes == "Y" || retakes == "yes");
        }

    }
}

