﻿using System;

namespace Party_Project
{
    public abstract class Party
    {
        public int Catering { get; set; }
        public int PerPerson { get; set; }
        public int Decoration { get; set; }
        public string Venue { get; set; }
        public DateTime DateTime { get; set; }
        public string PakageType { get; set; }
        public int NumOfPeople { get; set; }
        public float ServiceCharge { get; set; }

        public abstract void CalExpenditure(String venuName, float serviceCharge) ;
        public abstract void CalPersonExpenditure();
        public void display()
        {
            float serCharge = (ServiceCharge * (Catering + Decoration))*NumOfPeople / 100;
            Console.WriteLine("*******************Generated Recipte*************************");
            Console.WriteLine("Hotel Name           :" +Venue);
            Console.WriteLine("PackageType          :" + PakageType);
            Console.WriteLine("Date                 :" +DateTime);
            Console.WriteLine("Person               :" + NumOfPeople);
            Console.WriteLine("Cost of a Person     :" + (serCharge + Decoration + Catering)/NumOfPeople);
            Console.WriteLine("Catering             :" + Catering);
            Console.WriteLine("decoration           :" + Decoration);
            Console.WriteLine("ServiceCharge        :" +serCharge);
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("Grand Total  :" +(serCharge+Decoration+Catering));
            Console.WriteLine("--------------------------------------------------------------");


        }

    }
}