﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Party_Project
{
    class Venue
    {
        public void SelectVenue()
        {
            Package packObj = new Package();
            int selVenue;
            string retakes = null;
            Console.WriteLine("\n");
            do
            {
                Console.WriteLine("<<<< There are some selected venue we have organise party as per your requirement>>>>>\n" + "1.Taj Palace\n" + "2.Raddison Blue\n" + "3.The Grand Sobha");
                Console.WriteLine("Please! Enter your input on Numbers Like: 1,2 & 3");
               
                if (Int32.TryParse(Console.ReadLine(), out selVenue)) //checking integer or not
                {
                    if (selVenue == 1)
                    {
                        packObj.PackageSelector("Taj Palace");
                    }
                    else if (selVenue == 2)
                    {
                        packObj.PackageSelector("Radission blue");
                    }
                    else if (selVenue == 3)
                    {
                        packObj.PackageSelector("The Grand Shobha");
                    }
                    else
                    {
                        Console.WriteLine("WRONG!!! you entered an invalid");
                        Console.WriteLine("Please! Enter a correct input");
                        SelectVenue();
                    }
                }

                else
                {
                    Console.WriteLine("WRONG!!! you entered an invalid choice");
                    Console.WriteLine("Do you want to CONTINUE!! Please your suggestion in the y for Yes ");
                    retakes = Console.ReadLine();
                }

                } while (retakes == "y" || retakes == "Y" || retakes == "yes") ;
            }
    }

    }
