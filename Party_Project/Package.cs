﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Party_Project
{
    public class Package
    {
        float priceOfPackage;
        public void Silver(string venueName)
        {
            priceOfPackage = 1.6f;
            Selecter selectObj=new Selecter();
            selectObj.SelectEvent(venueName,priceOfPackage);
        }
        public void Gold(string venueName)
        {
            priceOfPackage = 2.4f;
            Selecter selectObj = new Selecter();
            selectObj.SelectEvent(venueName, priceOfPackage);
        }
        public void Platinum(string venueName)
        {
            priceOfPackage = 3.5f;
            Selecter selectObj = new Selecter();
            selectObj.SelectEvent(venueName, priceOfPackage);
        }

        public void PackageSelector(string venueName)
        {
            int selPackage;
            string retakes = null;
            Console.WriteLine("\n");
            do
            {
                Console.WriteLine("<<<< There are some selected pakages we have organise as per your requirement>>>>>\n" + "1.Silver Package\n" + "2.Gold Package\n" + "3.Platinum Package");
                Console.WriteLine("Please! Enter your input on Numbers Like: 1,2 & 3");
                string selectPackage = Console.ReadLine();
                if (Int32.TryParse(selectPackage, out selPackage)) //checking integer or not
                {
                    
                        if (selPackage == 1)
                        {
                            Silver(venueName);
                        }
                        else if (selPackage == 2)
                        {
                            Gold(venueName);
                        }
                        else if (selPackage == 3)
                        {
                            Platinum(venueName);
                        }
                        else
                        {
                            Console.WriteLine("WRONG!!! you entered an invalid");
                            Console.WriteLine("Please! Enter a correct input");
                            PackageSelector(venueName);
                        }
                   
                }
                else
                {
                    Console.WriteLine("WRONG!!! you entered an invalid choice");
                    Console.WriteLine("Do you want to CONTINUE!! Please your suggestion in the y for Yes ");
                    retakes = Console.ReadLine();
                }

            } while (retakes == "y" || retakes == "Y" || retakes == "yes");
        }

    }
}