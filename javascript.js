//function for changing the content in any tag where it's id is demo and the value changed from 'paragraph changed' when the function is called 
function myFunction() {
    document.getElementById("demo").innerHTML = "Paragraph changed.";
}
//.value gives the value of the selected element 
var s = document.getElementById("demo").value;
//.value ste the value of the selected element
document.getElementById("demo").value='yash';
//document.forms it gives the value of the forms tags using its name value
var name = document.forms["form name"]["tag name"].value;
//styling using a particular id in javascript where its font size changes to 35px
document.getElementById("demo").style.fontSize = "35px";
//using display function we can hide the content and show the content  
document.getElementById("demo").style.display = "none";
document.getElementById("demo").style.display = "block";
//write method is used for writing the output in the html page
document.write("yash");
//it shows the alert pop up on the web page 
window.alert("this is my site");
//console.log prints the output on the console window of the site which is inspect by any one using the F12 tab
console.log('any value');
//var is used to declare the variable keyword of any type it is not type bounded it holds alltype of data
var a = 10;
//string can be return in double and single inverted codes
//every variable name must be begins with the alphabet,underscore and the dollar symbol
var firstName = 'yash'; var lastName = "yash";
//we can concate the values using just the + operator
//as per the naming convention we generally use the camel case 
var name = firstName + lastName;
//** operator is used to set the power on the selected variable
var x = 10;
x **= 5;
//++ operatior is used to increment the value by one -- decrement the value by one
var b = 10;
console.log(b++); //result is ten because of post increment
//but again we run
console.log(++b);//result is 12 because os increment value of the post and the pre increment value of the variable

console.log(b--); //result is 12 because of post decrement
//but again we run
console.log(++b);//result is 10 because os decrement value of the post and the pre decrement value of the variable
//this language having a special comparison operator '===' is used to check or matches the type and its value
var x = 1;
console.log(x === 1);//true
console.log(x === "1");//false
//where the == operator is always give the true value for the above to operations
//this language having a special comparison operator '!==' is used to check or matches the type and its value
var x = 1;
console.log(x !== 1);//flase
console.log(x !== "1");//true
//we can use ternary operator also 
var voteable = (age < 18) ? "Too young" : "Old enough";
//When comparing a string with a number, JavaScript will convert the string to a number when doing the comparison. An empty string converts to 0. A non-numeric string converts to NaN which is always false
console("2" > "12");//true
//typeof function gives the type name of the variable is used
var a = "yash";
console.log(typeof (a));//string
// four variables are created and assigned in a single go, 
// separated by commas
var myObj = new Object(),
    str = 'myString',
    rand = Math.random(),
    obj = new Object();

myObj.type = 'Dot syntax';
myObj['date created'] = 'String with space';
myObj[str] = 'String value';
myObj[rand] = 'Random Number';
myObj[obj] = 'Object';
myObj[''] = 'Even an empty string';

console.log(myObj);

//instanceof keyword is used to check the instance of class it is
var myString = new String();
var myDate = new Date();

console.log(myString instanceof Object);
console.log(myString instanceof Date);
console.log(myString instanceof String);
console.log(myDate instanceof Date);
console.log(myDate instanceof Object);
console.log(myDate instanceof String); 

//The yield keyword is used to pause and resume a generator function (function* or legacy generator function).
function* foo(index) {
    while (index < 5) {
        yield index++;
    }
}
//const keyword is used to make the variable contant
const iterator = foo(0);

console.log(iterator.next().value);// 0

console.log(iterator.next().value);//1

//we delete the object property by using delete keyword
var person = {
    firstname: "John",
    lastname: "Doe",
    age: 50,
    eyecolor: "blue"
};
delete person.age;

//let holds the function in a variable and the key value pair 
function* countAppleSales() {
    let saleList = [3, 7, 5]
    for (let i = 0; i < saleList.length; i++) {
        yield saleList[i]
    }
}

let appleStore = countAppleSales()  // Generator { }
console.log(appleStore.next())      // { value: 3, done: false }
console.log(appleStore.next())      // { value: 7, done: false }
console.log(appleStore.next())      // { value: 5, done: false }
//json object
var myObj = { "name": "John", "age": 31, "city": "New York", "place of birth": "ratlam" };
//string variable
var myJSON = JSON.stringify(myObj);
//The JSON.parse() method parses a string and returns a JavaScript object.
var obj = JSON.parse(myJSON);

var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.constructor;
fruits.length;
fruits.myUcase();
Array.prototype.myUcase = function () {
    for (i = 0; i < this.length; i++) {
        this[i] = this[i].toUpperCase();
    }
};
//concat()	Joins two or more arrays, and returns a copy of the joined arrays
var hege = ["Cecilie", "Lone"];
var stale = ["Emil", "Tobias", "Linus"];
var children = hege.concat(stale);

//copyWithin()	Copies array elements within the array, to and from specified positions
var fruits = ["Banana", "Orange", "Apple", "Mango", "Kiwi", "Papaya"];
document.getElementById("demo").innerHTML = fruits;

function myFunction() {
    document.getElementById("demo").innerHTML = fruits.copyWithin(2, 0, 2);
}
//entries()	Returns a key / value pair Array Iteration Object
var fruits = ["Banana", "Orange", "Apple", "Mango"];
var f = fruits.entries();

for (x of f) {
    document.getElementById("demo").innerHTML += x;
}
//every()	Checks if every element in an array pass a test
var ages = [32, 33, 16, 40];

function checkAdult(age) {
    return age >= 18;
}

function myFunction() {
    document.getElementById("demo").innerHTML = ages.every(checkAdult);
}
//fill()	Fill the elements in an array with a static value
fruits.fill("orange");
//filter()	Creates a new array with every element in an array that pass a test
var ages = [32, 33, 16, 40];

function checkAdult(age) {
    return age >= 18;
}

function myFunction() {
    document.getElementById("demo").innerHTML = ages.filter(checkAdult);
}
//find()	Returns the value of the first element in an array that pass a test
var ages = [3, 10, 18, 20];

function checkAdult(age) {
    return age >= 18;
}

function myFunction() {
    document.getElementById("demo").innerHTML = ages.find(checkAdult);
}
//findIndex()	Returns the index of the first element in an array that pass a test
var ages = [3, 10, 18, 20];

function checkAdult(age) {
    return age >= 18;
}

function myFunction() {
    document.getElementById("demo").innerHTML = ages.findIndex(checkAdult);
}
//forEach()	Calls a function for each array element
var fruits = ["apple", "orange", "cherry"];
fruits.forEach(myFunction);

function myFunction(item, index) {
    document.getElementById("demo").innerHTML += index + ":" + item + "<br>";
}
//from()	Creates an array from an object
var myArr = Array.from("ABCDEFG");//A,B,C,D,E,F
//includes()	Check if an array contains the specified element
var fruits = ["Banana", "Orange", "Apple", "Mango"];
var n = fruits.includes("Mango");//true
//indexOf()	Search the array for an element and returns its position
var fruits = ["Banana", "Orange", "Apple", "Mango"];
var a = fruits.indexOf("Apple");//index
//isArray()	Checks whether an object is an array
function myFunction() {
    var fruits = ["Banana", "Orange", "Apple", "Mango"];
    var x = document.getElementById("demo");
    x.innerHTML = Array.isArray(fruits);//boolean
}
//join()	Joins all elements of an array into a 
var fruits = ["Banana", "Orange", "Apple", "Mango"];
var energy = fruits.join();
//keys()	Returns a Array Iteration Object, containing the keys of the original array
var fruits = ["Banana", "Orange", "Apple", "Mango"];
var fk = fruits.keys();
//lastIndexOf()	Search the array for an element, starting at the end, and returns its position
var fruits = ["Banana", "Orange", "Apple", "Mango"];
var fk = fruits.lastIndexOf("Apple");
//map()	Creates a new array with the result of calling a function for each array element
var numbers = [4, 9, 16, 25];
var x = numbers.map(Math.sqrt)
//pop()	Removes the last element of an array, and returns that element
fruits.pop();
//push()	Adds new elements to the end of an array, and returns the new length
fruits.push("Kiwi");
//reduce()	Reduce the values of an array to a single value(going left - to - right)
//reduceRight()	Reduce the values of an array to a single value(going right - to - left)
//reverse()	Reverses the order of the elements in an array
//shift()	Removes the first element of an array, and returns that element
//slice()	Selects a part of an array, and returns the new array
//some()	Checks if any of the elements in an array pass a test
//sort()	Sorts the elements of an array
//splice()	Adds / Removes elements from an array
//toString()	Converts an array to a string, and returns the result
//unshift()	Adds new elements to the beginning of an array, and returns the new length
//valueOf()	Returns the primitive value of an array
//break	Terminates a switch or a loop
break;
//continue	Jumps out of a loop and starts at the top
continue;
//debugger	Stops the execution of JavaScript, and calls(if available) the debugging function
//do ... while	Executes a block of statements, and repeats the block, while a condition is true
var text = "";
var i = 0;
do {
    text += "The number is " + i;
    i++;
}
while (i < 5);
//for	Marks a block of statements to be executed, as long as a condition is true
let saleList = [3, 7, 5]
for (let i = 0; i < saleList.length; i++) {
    console.log(saleList[i]);
}
//if ... else Marks a block of statements to be executed, depending on a condition
for (let i = 0; i < 12; i++) {
    if (i == 1) { console.log(i); }
    else { continue; }
}
//return Exits a function
function add(a, b) { return a + b; }
console.log(add(2, 3));
//while
var text = "";
var i = 0;
while (i < 5) {
    text += "<br>The number is " + i;
    i++;
}
//switch	Marks a block of statements to be executed, depending on different cases
var text;
var fruits = document.getElementById("myInput").value;

switch (fruits) {
    case "Banana":
        text = "Banana is good!";
        break;
    case "Orange":
        text = "I am not a fan of orange.";
        break;
    case "Apple":
        text = "How you like them apples?";
        break;
    default:
        text = "I have never heard of that fruit...";
}
//try ... catch	Implements error handling to a block of statements
//type conversion
function isArray(myArray) {
    return myArray.constructor.toString().indexOf("Array") > -1;
}
function isArray(myArray) {
    return myArray.constructor === Array;
}
function isDate(myDate) {
    return myDate.constructor.toString().indexOf("Date") > -1;
}
function isDate(myDate) {
    return myDate.constructor === Date;
}

String(123)       // returns a string from a number literal 123
(123).toString() 
5 + null    // returns 5         because null is converted to 0
"5" + null  // returns "5null"   because null is converted to "null"
"5" + 2     // returns "52"      because 2 is converted to "2"
"5" - 2     // returns 3         because "5" is converted to 5
"5" * "2"   // returns 10        because "5" and "2" are converted to 5 and 2

//arrow function
hello = () => {
    return "Hello World!";
}
//by default return
hello = () => "Hello World!";
//with parameter
hello = (val) => "Hello " + val;
//without paranthesis
hello = val => "Hello " + val;

//callback function 
function functionOne(x) { alert(x); }

function functionTwo(var1, callback) {
    callback(var1);
}

functionTwo(2, functionOne);
//map in javascript
//new Map()  creates the map.
//map.set(key, value)  stores the value by the key.
//map.get(key)  returns the value by the key, undefined if key doesnt exist in map.
//map.has(key)  returns true if the key exists, false otherwise.
// map.delete(key)  removes the value by the key.
//map.clear()  removes everything from the map.
//map.size  returns the current element count.
let map = new Map();

map.set('1', 'str1');   // a string key
map.set(1, 'num1');     // a numeric key
map.set(true, 'bool1'); // a boolean key
alert(map.get(1)); // 'num1'
alert(map.get('1')); // 'str1'

alert(map.size); // 3

let john = { name: "John" };

// for every user, let's store their visits count
let visitsCountMap = new Map();

// john is the key for the map
visitsCountMap.set(john, 123);

alert(visitsCountMap.get(john)); // 123

//iteration of map
let recipeMap = new Map([
    ['cucumber', 500],
    ['tomatoes', 350],
    ['onion', 50]
]);

// iterate over keys (vegetables)
for (let vegetable of recipeMap.keys()) {
    alert(vegetable); // cucumber, tomatoes, onion
}

// iterate over values (amounts)
for (let amount of recipeMap.values()) {
    alert(amount); // 500, 350, 50
}

// iterate over [key, value] entries
for (let entry of recipeMap) { // the same as of recipeMap.entries()
    alert(entry); // cucumber,500 (and so on)
}
//obj to map
let obj = {  name: "John",  age: 30 };

let map = new Map(Object.entries(obj));

alert(map.get('name')); // John

//set in javascript
//new Set(iterable)  creates the set, and if an iterable object is provided(usually an array), copies values from it into the set.
//set.add(value)  adds a value, returns the set itself.
//set.delete(value)  removes the value, returns true if value existed at the moment of the call, otherwise false.
//set.has(value)  returns true if the value exists in the set, otherwise false.
//set.clear()  removes everything from the set.
//set.size  is the elements count.
let set = new Set();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

// visits, some users come multiple times
set.add(john);
set.add(pete);
set.add(mary);
set.add(john);
set.add(mary);

let set = new Set(["oranges", "apples", "bananas"]);

for (let value of set) console.log(value);

//date and time metods
//getmonth gives the current month
var d = new Date();
document.getElementById("demo").innerHTML = d.getMonth();
//getdate gives the current date
document.getElementById("demo").innerHTML = d.getDate();
//getminute gives the current minute
document.getElementById("demo").innerHTML = d.getMinutes()
//getHours gives the current hour
document.getElementById("demo").innerHTML = d.getHours();
//getday gives the current day 
document.getElementById("demo").innerHTML = d.getDay();