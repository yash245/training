﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            char ch_value = 'a';
            int ch_int = ch_value;
            long ch_lo = ch_value;
            long int_lo = ch_int;
            float lon_flo = ch_lo;
            float ch_flo = ch_valuec;
            double ch_dou = ch_flo;
            Console.WriteLine("Type conversion");
            Console.WriteLine("Implicit type conversions");
            Console.WriteLine("Value to char is " + ch_value);
            Console.WriteLine("char value to int is" + ch_int);
            Console.WriteLine("char to long is" + ch_lo);
            Console.WriteLine("long to float is" + lon_flo);
            Console.WriteLine("float to double is" + ch_dou);
            Console.WriteLine("int to long is" + int_lo);
            Console.WriteLine("char to float is" + ch_flo);


            Console.WriteLine("\n\texplicit type conversions");

            double dou = 263177.56523;
            float do_fl = (float)dou;
            long do_lo = (long)dou;
            int do_int = (int)dou;
            Console.WriteLine("double=" + dou);
            Console.WriteLine("float=" + do_fl);
            Console.WriteLine("long=" + do_lo);
            Console.WriteLine("int=" + do_int);

            Console.WriteLine("\nother conversions");
            Console.WriteLine("double to int 32=" + Convert.ToInt32(dou));
            Console.WriteLine("float to uint32=" + Convert.ToUInt32(do_fl));
            Console.WriteLine("Flaot to string=" + Convert.ToString(do_fl));
            Console.WriteLine("int to double= " + Convert.ToDouble(do_int));
            
            Console.ReadKey();
        }
    }
}
