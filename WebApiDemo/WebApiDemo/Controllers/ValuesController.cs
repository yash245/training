﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiDemo.Controllers
{
    public class ValuesController : ApiController
    {
        static List<string> week = new List<string>() { "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" };
        // GET api/values
        public IEnumerable<string> Get()
        {
            return week;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return week[id];
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
            week.Add(value);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
            week[id] = value;
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            week.RemoveAt(id);
        }
    }
}
