﻿function sayHello() {
    const compiler = (document.getElementById("compiler") as HTMLInputElement).value;
    const framework = (document.getElementById("framework") as HTMLInputElement).value;
    return `Hello from ${compiler} and ${framework}!`;
}

//boolean
let bool: boolean = false;
//Number we  define number at any form dec,hexa,octa,or binary form
let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;
//string 
let color: string = "blue";
color = 'red';
//for embedded expression we use backtick sign
let fullName: string = `Bob Bobbington`;
let age: number = 37;
let sentence: string =` Hello, my name is ${fullName}.
I'll be ${ age + 1} years old next month.`;
//Array is to be initialized by[]
let list: number[] = [1,2,3,4];
//For generic type 
let List: Array<number> = [1, 2, 3, 4];
//Tuple types allow you to express an array with a fixed number of elements whose types are known, but need not be the same
let a: [string, number];
a = ["yash", 22];//correct but vice versa is not
console.log(a[0].substring(1)); // OK

let tuple: [number, string, boolean] = [7, "hello", true];

let [num,str,b] = tuple; // num: number, str: string, b: boolean
//enum are used to hold the universal entity
enum Color { green, red, blue };
let c = Color.green;
console.log(c);
//generally the value of enum are index by 0 but you can change it by giving your index
enum Colour { green=1, red, blue };
let d = Colour[3];
console.log(d);
//Any is used for type independent variable where we change its type at any time
let notSure: any = 4;
notSure = "maybe a string instead";
notSure = false; // okay, definitely a boolean
//this also done by an object but the difference is we can used the default methods on this variable 
//like ifItExist() or if Fixed();
notSure.isFixed();
//void is used to not return value from a function 
function warnUser(): void {
    console.log("This is my warning message");
}
//null and undefined
// Not much else we can assign to these variables!
let u: undefined = undefined;
let n: null = null;
//never
//never is the return type for a function expression or an arrow function expression that always throws an exception or one that never returns
// Function returning never must have unreachable end point
function error(message: string): never {
    throw new Error(message);
}
function fail() {
    return error("Something failed"); //doubt
}

//object is a type that represents the non-primitive type
declare function create(o: object | null): void;

create({ prop: 0 }); // OK
create(null); // OK
//create(42); // Error

//type assertion
let someValue: any = "this is a string";
let strLength: number = (<string>someValue).length;
let stringLength: number = (someValue as string).length;

//var is used for defining a variable that hold all type of datatype but in different diff variables
var variable = 10;

//scope variable
function f(shouldInitialize: boolean) {
    if (shouldInitialize) {
        var x = 10;
    }

    return x;
}

f(true);  // returns '10'
f(false); // returns 'undefined'

//setTimeout
for (var i = 0; i < 10; i++) {
    setTimeout(function () { console.log(i); }, 100 * i);
}// output 10

//const keyword
const numLivesForCat = 9;
const kitty = {
    name: "Aurora",
    numLives: numLivesForCat,
}
// Error kitty = { name: "Danielle",  numLives: numLivesForCat};

// all "okay"
kitty.name = "Rory";
kitty.name = "Kitty";
kitty.name = "Cat";
kitty.numLives--;

//functions
// Named function
function add(x, y) {
    return x + y;
}

// Anonymous function
let myAdd = function (x, y) { return x + y; };

//extra feature in js
let z = 100;

function addToZ(x, y) {
    return x + y + z;
}
console.log(addToZ(10, 20));//130

//type centric in function
// myAdd has the full function type
let myAddition = function (x: number, y: number): number { return x + y; };

// The parameters 'x' and 'y' have the type number
let myAdding: (baseValue: number, increment: number) => number =
    function (x, y) { return x + y; };

//optional parameter is assign by the ?
function buildName(firstName: string, lastName?: string) {
    if (lastName)
        return firstName + " " + lastName;
    else
        return firstName;
}

let result1 = buildName("Bob");                  // works correctly now
//let result2 = buildName("Bob", "Adams", "Sr.");  // error, too many parameters
let result3 = buildName("Bob", "Adams");         // ah, just right

function Name(firstName = "Will", lastName: string) {
    return firstName + " " + lastName;
}

//let result1 = Name("Bob");                  // error, too few parameters
//let result2 = Name("Bob", "Adams", "Sr.");  // error, too many parameters
let result2 = Name("Bob", "Adams");         // okay and returns "Bob Adams"
let result4 = Name(undefined, "Adams");     // okay and returns "Will Adams"

//Rest parameter
function restName(firstName: string, ...restOfName: string[]) {
    return firstName + " " + restOfName.join(" ");
}

// employeeName will be "Joseph Samuel Lucas MacKinzie"
let employeeName = restName("Joseph", "Samuel", "Lucas", "MacKinzie");
//this keyword
let deck = {
    suits: ["hearts", "spades", "clubs", "diamonds"],
    cards: Array(52),
    createCardPicker: function () {
        // NOTE: the line below is now an arrow function, allowing us to capture 'this' right here
        return () => {
            let pickedCard = Math.floor(Math.random() * 52);
            let pickedSuit = Math.floor(pickedCard / 13);

            return { suit: this.suits[pickedSuit], card: pickedCard % 13 };
        }
    }
}

let cardPicker = deck.createCardPicker();
let pickedCard = cardPicker();
console.log("card: " + pickedCard.card + " of " + pickedCard.suit);

//overload function
let suits = ["hearts", "spades", "clubs", "diamonds"];

function pickCard(x: { suit: string; card: number; }[]): number;
function pickCard(x: number): { suit: string; card: number; };
function pickCard(x): any {
    // Check to see if we're working with an object/array
    // if so, they gave us the deck and we'll pick the card
    if (typeof x == "object") {
        let pickedCard = Math.floor(Math.random() * x.length);
        return pickedCard;
    }
    // Otherwise just let them pick the card
    else if (typeof x == "number") {
        let pickedSuit = Math.floor(x / 13);
        return { suit: suits[pickedSuit], card: x % 13 };
    }
}

let myDeck = [{ suit: "diamonds", card: 2 }, { suit: "spades", card: 10 }, { suit: "hearts", card: 4 }];
let pickedCard1 = myDeck[pickCard(myDeck)];
alert("card: " + pickedCard1.card + " of " + pickedCard1.suit);

let pickedCard2 = pickCard(15);
alert("card: " + pickedCard2.card + " of " + pickedCard2.suit);

//class
class Person {
    protected name: string;
    protected constructor(theName: string) { this.name = theName; }
}

// Employee can extend Person
class Employee extends Person {
    private department: string;

    constructor(name: string, department: string) {
        super(name);
        this.department = department;
    }

    public getElevatorPitch() {
        return `Hello, my name is ${this.name} and I work in ${this.department}.`;
    }
}

let howard = new Employee("Howard", "Sales");
//let john = new Person("John"); // Error: The 'Person' constructor is protected

//readonly modifier
class Octopus {
    readonly name: string;
    readonly numberOfLegs: number = 8;
    constructor(theName: string) {
        this.name = theName;
    }
}
let dad = new Octopus("Man with the 8 strong legs");
//dad.name = "Man with the 3-piece suit"; // error! name is readonly.

//abstract class
abstract class Department {

    constructor(public name: string) {
    }

    printName(): void {
        console.log("Department name: " + this.name);
    }

    abstract printMeeting(): void; // must be implemented in derived classes
}

class AccountingDepartment extends Department {

    constructor() {
        super("Accounting and Auditing"); // constructors in derived classes must call super()
    }

    printMeeting(): void {
        console.log("The Accounting Department meets each Monday at 10am.");
    }

    generateReports(): void {
        console.log("Generating accounting reports...");
    }
}

let department: Department; // ok to create a reference to an abstract type
//department = new Department(); // error: cannot create an instance of an abstract class
department = new AccountingDepartment(); // ok to create and assign a non-abstract subclass
department.printName();
department.printMeeting();
//department.generateReports(); // error: method doesn't exist on declared abstract type

//inheritance
class Animal {
    name: string;
    constructor(theName: string) { this.name = theName; }
    move(distanceInMeters: number = 0) {
        console.log(`${this.name} moved ${distanceInMeters}m.`);
    }
}

class Snake extends Animal {
    constructor(name: string) { super(name); }
    move(distanceInMeters = 5) {
        console.log("Slithering...");
        super.move(distanceInMeters);
    }
}

class Horse extends Animal {
    constructor(name: string) { super(name); }
    move(distanceInMeters = 45) {
        console.log("Galloping...");
        super.move(distanceInMeters);
    }
}

let sam = new Snake("Sammy the Python");
let tom: Animal = new Horse("Tommy the Palomino");

sam.move();
tom.move(34);

//Advance techniques
class Greeter {
    greeting: string;
    constructor(message: string) {
        this.greeting = message;
    }
    greet() {
        return "Hello, " + this.greeting;
    }
}

let greeter: Greeter;
greeter = new Greeter("world");
console.log(greeter.greet()); // "Hello, world"

//interfaces
interface SquareConfig {
    color?: string;
    width?: number;
}

function createSquare(config: SquareConfig): { color: string; area: number } {
    let newSquare = { color: "white", area: 100 };
    if (config.color) {
        // Error: Property 'clor' does not exist on type 'SquareConfig'
        newSquare.color = config.color;
    }
    if (config.width) {
        newSquare.area = config.width * config.width;
    }
    return newSquare;
}

let mySquare = createSquare({ color: "black" });

//Readonly properties 
interface Point {
    readonly x: number;
    readonly y: number;
}

//readonly vs const readonly is used in declaring prop and cont for variable
//interface in class
interface ClockInterface {
    currentTime: Date;
    setTime(d: Date): void;
}

class Clock implements ClockInterface {
    currentTime: Date = new Date();
    setTime(d: Date) {
        this.currentTime = d;
    }
    constructor(h: number, m: number) { }
}
//Generics
//working with generic type
function loggingIdentity<T>(arg: T): T {
    //console.log(arg.length);  // Error= T doesn't have .length
    return arg;
}

//gneric classes
class GenericNumber<T> {
    zeroValue: T;
    add: (x: T, y: T) => T;
}
let myGenericNumber = new GenericNumber<number>();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) { return x + y; };

//Generic Constraints
interface Lengthwise {
    length: number;
}

function loggingIdentities<T extends Lengthwise>(arg: T): T {
    console.log(arg.length);  // Now we know it has a .length property, so no more error
    return arg;
}

//Enums
//enums can make it easier to document intent, or create a set of distinct cases.
//TypeScript provides both numeric and string - based enums
//Numeric enums
enum Direction {
    Up = 1,
    Down,
    Left,
    Right,
}
//String enums
enum Directions {
    Up = "UP",
    Down = "DOWN",
    Left = "LEFT",
    Right = "RIGHT",
}

// Heterogeneous enums
// Technically enums can be mixed with string and numeric members

enum BooleanLikeHeterogeneousEnum {
    No = 0,
    Yes = "YES",
}

//iterators

let list1 = [4, 5, 6];
// for in
for (let i in list1) {
    console.log(i); // "0", "1", "2",
}
// for of
for (let i of list1) {
    console.log(i); // "4", "5", "6"
}
//for
for (var _i = 0; _i < list1.length; _i++) {
    var number = list1[_i];
    console.log(number);
}