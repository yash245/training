var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
function sayHello() {
    var compiler = document.getElementById("compiler").value;
    var framework = document.getElementById("framework").value;
    return "Hello from " + compiler + " and " + framework + "!";
}
//boolean
var bool = false;
//Number we  define number at any form dec,hexa,octa,or binary form
var decimal = 6;
var hex = 0xf00d;
var binary = 10;
var octal = 484;
//string 
var color = "blue";
color = 'red';
//for embedded expression we use backtick sign
var fullName = "Bob Bobbington";
var age = 37;
var sentence = " Hello, my name is " + fullName + ".\nI'll be " + (age + 1) + " years old next month.";
//Array is to be initialized by[]
var list = [1, 2, 3, 4];
//For generic type 
var List = [1, 2, 3, 4];
//Tuple types allow you to express an array with a fixed number of elements whose types are known, but need not be the same
var a;
a = ["yash", 22]; //correct but vice versa is not
console.log(a[0].substring(1)); // OK
var tuple = [7, "hello", true];
var num = tuple[0], str = tuple[1], b = tuple[2]; // num: number, str: string, b: boolean
//enum are used to hold the universal entity
var Color;
(function (Color) {
    Color[Color["green"] = 0] = "green";
    Color[Color["red"] = 1] = "red";
    Color[Color["blue"] = 2] = "blue";
})(Color || (Color = {}));
;
var c = Color.green;
console.log(c);
//generally the value of enum are index by 0 but you can change it by giving your index
var Colour;
(function (Colour) {
    Colour[Colour["green"] = 1] = "green";
    Colour[Colour["red"] = 2] = "red";
    Colour[Colour["blue"] = 3] = "blue";
})(Colour || (Colour = {}));
;
var d = Colour[3];
console.log(d);
//Any is used for type independent variable where we change its type at any time
var notSure = 4;
notSure = "maybe a string instead";
notSure = false; // okay, definitely a boolean
//this also done by an object but the difference is we can used the default methods on this variable 
//like ifItExist() or if Fixed();
notSure.isFixed();
//void is used to not return value from a function 
function warnUser() {
    console.log("This is my warning message");
}
//null and undefined
// Not much else we can assign to these variables!
var u = undefined;
var n = null;
//never
//never is the return type for a function expression or an arrow function expression that always throws an exception or one that never returns
// Function returning never must have unreachable end point
function error(message) {
    throw new Error(message);
}
function fail() {
    return error("Something failed"); //doubt
}
create({ prop: 0 }); // OK
create(null); // OK
//create(42); // Error
//type assertion
var someValue = "this is a string";
var strLength = someValue.length;
var stringLength = someValue.length;
//var is used for defining a variable that hold all type of datatype but in different diff variables
var variable = 10;
//scope variable
function f(shouldInitialize) {
    if (shouldInitialize) {
        var x = 10;
    }
    return x;
}
f(true); // returns '10'
f(false); // returns 'undefined'
//setTimeout
for (var i = 0; i < 10; i++) {
    setTimeout(function () { console.log(i); }, 100 * i);
} // output 10
//const keyword
var numLivesForCat = 9;
var kitty = {
    name: "Aurora",
    numLives: numLivesForCat,
};
// Error kitty = { name: "Danielle",  numLives: numLivesForCat};
// all "okay"
kitty.name = "Rory";
kitty.name = "Kitty";
kitty.name = "Cat";
kitty.numLives--;
//functions
// Named function
function add(x, y) {
    return x + y;
}
// Anonymous function
var myAdd = function (x, y) { return x + y; };
//extra feature in js
var z = 100;
function addToZ(x, y) {
    return x + y + z;
}
console.log(addToZ(10, 20)); //130
//type centric in function
// myAdd has the full function type
var myAddition = function (x, y) { return x + y; };
// The parameters 'x' and 'y' have the type number
var myAdding = function (x, y) { return x + y; };
//optional parameter is assign by the ?
function buildName(firstName, lastName) {
    if (lastName)
        return firstName + " " + lastName;
    else
        return firstName;
}
var result1 = buildName("Bob"); // works correctly now
//let result2 = buildName("Bob", "Adams", "Sr.");  // error, too many parameters
var result3 = buildName("Bob", "Adams"); // ah, just right
function Name(firstName, lastName) {
    if (firstName === void 0) { firstName = "Will"; }
    return firstName + " " + lastName;
}
//let result1 = Name("Bob");                  // error, too few parameters
//let result2 = Name("Bob", "Adams", "Sr.");  // error, too many parameters
var result2 = Name("Bob", "Adams"); // okay and returns "Bob Adams"
var result4 = Name(undefined, "Adams"); // okay and returns "Will Adams"
//Rest parameter
function restName(firstName) {
    var restOfName = [];
    for (var _a = 1; _a < arguments.length; _a++) {
        restOfName[_a - 1] = arguments[_a];
    }
    return firstName + " " + restOfName.join(" ");
}
// employeeName will be "Joseph Samuel Lucas MacKinzie"
var employeeName = restName("Joseph", "Samuel", "Lucas", "MacKinzie");
//this keyword
var deck = {
    suits: ["hearts", "spades", "clubs", "diamonds"],
    cards: Array(52),
    createCardPicker: function () {
        var _this = this;
        // NOTE: the line below is now an arrow function, allowing us to capture 'this' right here
        return function () {
            var pickedCard = Math.floor(Math.random() * 52);
            var pickedSuit = Math.floor(pickedCard / 13);
            return { suit: _this.suits[pickedSuit], card: pickedCard % 13 };
        };
    }
};
var cardPicker = deck.createCardPicker();
var pickedCard = cardPicker();
console.log("card: " + pickedCard.card + " of " + pickedCard.suit);
//overload function
var suits = ["hearts", "spades", "clubs", "diamonds"];
function pickCard(x) {
    // Check to see if we're working with an object/array
    // if so, they gave us the deck and we'll pick the card
    if (typeof x == "object") {
        var pickedCard_1 = Math.floor(Math.random() * x.length);
        return pickedCard_1;
    }
    // Otherwise just let them pick the card
    else if (typeof x == "number") {
        var pickedSuit = Math.floor(x / 13);
        return { suit: suits[pickedSuit], card: x % 13 };
    }
}
var myDeck = [{ suit: "diamonds", card: 2 }, { suit: "spades", card: 10 }, { suit: "hearts", card: 4 }];
var pickedCard1 = myDeck[pickCard(myDeck)];
alert("card: " + pickedCard1.card + " of " + pickedCard1.suit);
var pickedCard2 = pickCard(15);
alert("card: " + pickedCard2.card + " of " + pickedCard2.suit);
//class
var Person = /** @class */ (function () {
    function Person(theName) {
        this.name = theName;
    }
    return Person;
}());
// Employee can extend Person
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(name, department) {
        var _this = _super.call(this, name) || this;
        _this.department = department;
        return _this;
    }
    Employee.prototype.getElevatorPitch = function () {
        return "Hello, my name is " + this.name + " and I work in " + this.department + ".";
    };
    return Employee;
}(Person));
var howard = new Employee("Howard", "Sales");
//let john = new Person("John"); // Error: The 'Person' constructor is protected
//readonly modifier
var Octopus = /** @class */ (function () {
    function Octopus(theName) {
        this.numberOfLegs = 8;
        this.name = theName;
    }
    return Octopus;
}());
var dad = new Octopus("Man with the 8 strong legs");
//dad.name = "Man with the 3-piece suit"; // error! name is readonly.
//abstract class
var Department = /** @class */ (function () {
    function Department(name) {
        this.name = name;
    }
    Department.prototype.printName = function () {
        console.log("Department name: " + this.name);
    };
    return Department;
}());
var AccountingDepartment = /** @class */ (function (_super) {
    __extends(AccountingDepartment, _super);
    function AccountingDepartment() {
        return _super.call(this, "Accounting and Auditing") || this;
    }
    AccountingDepartment.prototype.printMeeting = function () {
        console.log("The Accounting Department meets each Monday at 10am.");
    };
    AccountingDepartment.prototype.generateReports = function () {
        console.log("Generating accounting reports...");
    };
    return AccountingDepartment;
}(Department));
var department; // ok to create a reference to an abstract type
//department = new Department(); // error: cannot create an instance of an abstract class
department = new AccountingDepartment(); // ok to create and assign a non-abstract subclass
department.printName();
department.printMeeting();
//department.generateReports(); // error: method doesn't exist on declared abstract type
//inheritance
var Animal = /** @class */ (function () {
    function Animal(theName) {
        this.name = theName;
    }
    Animal.prototype.move = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 0; }
        console.log(this.name + " moved " + distanceInMeters + "m.");
    };
    return Animal;
}());
var Snake = /** @class */ (function (_super) {
    __extends(Snake, _super);
    function Snake(name) {
        return _super.call(this, name) || this;
    }
    Snake.prototype.move = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 5; }
        console.log("Slithering...");
        _super.prototype.move.call(this, distanceInMeters);
    };
    return Snake;
}(Animal));
var Horse = /** @class */ (function (_super) {
    __extends(Horse, _super);
    function Horse(name) {
        return _super.call(this, name) || this;
    }
    Horse.prototype.move = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 45; }
        console.log("Galloping...");
        _super.prototype.move.call(this, distanceInMeters);
    };
    return Horse;
}(Animal));
var sam = new Snake("Sammy the Python");
var tom = new Horse("Tommy the Palomino");
sam.move();
tom.move(34);
//Advance techniques
var Greeter = /** @class */ (function () {
    function Greeter(message) {
        this.greeting = message;
    }
    Greeter.prototype.greet = function () {
        return "Hello, " + this.greeting;
    };
    return Greeter;
}());
var greeter;
greeter = new Greeter("world");
console.log(greeter.greet()); // "Hello, world"
function createSquare(config) {
    var newSquare = { color: "white", area: 100 };
    if (config.color) {
        // Error: Property 'clor' does not exist on type 'SquareConfig'
        newSquare.color = config.color;
    }
    if (config.width) {
        newSquare.area = config.width * config.width;
    }
    return newSquare;
}
var mySquare = createSquare({ color: "black" });
var Clock = /** @class */ (function () {
    function Clock(h, m) {
        this.currentTime = new Date();
    }
    Clock.prototype.setTime = function (d) {
        this.currentTime = d;
    };
    return Clock;
}());
//Generics
//working with generic type
function loggingIdentity(arg) {
    //console.log(arg.length);  // Error= T doesn't have .length
    return arg;
}
//gneric classes
var GenericNumber = /** @class */ (function () {
    function GenericNumber() {
    }
    return GenericNumber;
}());
var myGenericNumber = new GenericNumber();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) { return x + y; };
function loggingIdentities(arg) {
    console.log(arg.length); // Now we know it has a .length property, so no more error
    return arg;
}
//Enums
//enums can make it easier to document intent, or create a set of distinct cases.
//TypeScript provides both numeric and string - based enums
//Numeric enums
var Direction;
(function (Direction) {
    Direction[Direction["Up"] = 1] = "Up";
    Direction[Direction["Down"] = 2] = "Down";
    Direction[Direction["Left"] = 3] = "Left";
    Direction[Direction["Right"] = 4] = "Right";
})(Direction || (Direction = {}));
//String enums
var Directions;
(function (Directions) {
    Directions["Up"] = "UP";
    Directions["Down"] = "DOWN";
    Directions["Left"] = "LEFT";
    Directions["Right"] = "RIGHT";
})(Directions || (Directions = {}));
// Heterogeneous enums
// Technically enums can be mixed with string and numeric members
var BooleanLikeHeterogeneousEnum;
(function (BooleanLikeHeterogeneousEnum) {
    BooleanLikeHeterogeneousEnum[BooleanLikeHeterogeneousEnum["No"] = 0] = "No";
    BooleanLikeHeterogeneousEnum["Yes"] = "YES";
})(BooleanLikeHeterogeneousEnum || (BooleanLikeHeterogeneousEnum = {}));
//iterators
var list1 = [4, 5, 6];
// for in
for (var i_1 in list1) {
    console.log(i_1); // "0", "1", "2",
}
// for of
for (var _a = 0, list1_1 = list1; _a < list1_1.length; _a++) {
    var i_2 = list1_1[_a];
    console.log(i_2); // "4", "5", "6"
}
//for
for (var _i = 0; _i < list1.length; _i++) {
    var number = list1[_i];
    console.log(number);
}
//# sourceMappingURL=demo.js.map