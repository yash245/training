﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Xunit;

namespace MVCProject.Models
{
    public class Employee
    {
        public int empId { get; set; }
        [Required]
        public string empName { get; set; }
        [Required]
        public string dob { get; set; }
        [Required]
        public string phone { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string addr { get; set; }
        [Required]
        public City city { get; set; }
        public string joindate { get; set; }
        [Required]
        public string zip { get; set; }
        [Required]
        public Department dept { get; set; }
        [Required]
        public State state { get; set; }
    }
    public enum Department
    { 
        IT,ADMIN,QA,BA,HR
    }
    public enum State
    { Chennai,Delhi,Mumbai}
    public enum City
    { Chennai,NewDelhi,OldDelhi,Maharashtra, Mumbai }
}