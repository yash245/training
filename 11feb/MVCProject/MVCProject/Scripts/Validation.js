﻿
$(document).ready(function () {
	$("#nameParagraph").hide();
	$("#dobParagraph").hide();
	$("#numberParagraph").hide();
	$("#emailParagraph").hide();
	$("#deptParagraph").hide();
	$("#addressParagraph").hide();
	$("#stateParagraph").hide();
	$("#cityParagraph").hide();
	$("#zipParagraph").hide();
	$("#table").hide();

	var user_err = true;
	var citiesByState = {
		Chennai: ["Chennai"],
		Maharashtra: ["Mumbai", "Pune", "Nagpur"],
		Delhi: ["New Delhi", "Old Delhi"]
	}
	$("#state").change(function () {
		var state = $('#state').val();
		if (state == null) {
			$("#stateParagraph").show();
			$('#stateParagraph').text('State cant be Empty');
			$("#stateParagraph").css("color", "red");
		}
		else if (state.length == 0) {
			$('#city').text("<option>Select City</option>");
		}
		else {
			var citiesOptions = "";
			for (cityId in citiesByState[state]) {
				citiesOptions += "<option>" + citiesByState[state][cityId] + "</option>";
			}
			$('#city').html(citiesOptions);
			$('#stateParagraph').hide();
		}
	});

	$('#empName').keyup(function () {
		CheckName();
	});
	$('#date').change(function () {
		DobCheck();
	});
	$('#phone').keyup(function () {
		PhoneCheck();
	});
	$('#email').keyup(function () {
		EmailCheck();
	});
	$('#dept').change(function () {
		CheckDept();
	});
	$('#addr').keyup(function () {
		CheckAddress();
	});
	$('#zip').keyup(function () {
		CheckZip();
	});
	$('#click').click(function () {
		var user = $('#empName').val();
		if (user.length == 0) {
			$("#nameParagraph").show();
			$("#nameParagraph").html("*please fill the Name");
			$("#nameParagraph").css("color", "red");

		}
		var adr = $('#addr').val();
		if (adr.length == 0) {
			$("#addressParagraph").show();
			$("#addressParagraph").html("*Please fill the Address");
			$("#addressParagraph").css("color", "red");

		}
		var dept = $('#dept').val();
		if (dept == null) {
			$("#deptParagraph").show();
			$("#deptParagraph").html("*please select the Department");
			$("#deptParagraph").css("color", "red");
		}
		var zip = $('#zip').val();
		if (zip.length == 0) {
			$("#zipParagraph").show();
			$("#zipParagraph").html("*Please fill the Zip code");
			$("#zipParagraph").css("color", "red");

		}
		var dob = $('#dob').val();
		if (dob.length == 0) {
			$("#dobParagraph").show();
			$("#dobParagraph").html("**Please fill the DOB");
			$("#dobParagraph").css("color", "red");

		}
		var state = $('#state').val();
		if (state == null) {
			$("#stateParagraph").show();
			$('#stateParagraph').text('State cant be Empty');
			$("#stateParagraph").css("color", "red");
		}
		var Phone = $('#phone').val();
		if (Phone.length == 0) {
			$("#numberParagraph").show();
			$("#numberParagraph").html("*Please fill the Phone no.");
			$("#numberParagraph").css("color", "red");

		}
		var email = $('#email').val();
		if (email.length == 0) {
			$("#emailParagraph").show();
			$("#emailParagraph").html("*Please fill the Email address");
			$("#emailParagraph").css("color", "red");

		}
		if ($('#empName').val() == "" || $('#dob').val() == "" || $('#email').val() == "" || $('#phone').val() == "" || $('#dept').val() == null || $('#state').val() == null || $('#zip').val() == "" || $('#addr').val() == "")
		{
			return false;
		}
	});
	function CheckName() {
		var user = $('#empName').val();
		if (user.length == 0) {
			$("#nameParagraph").show();
			$("#nameParagraph").html("*please fill the Name");
			$("#nameParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		else {
			$("#nameParagraph").hide();
		}
	}

	function DobCheck() {
		var dob = $('#dob').val();
		if (dob.length == 0) {
			$("#dobParagraph").show();
			$("#dobParagraph").html("**Please fill the DOB");
			$("#dobParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		else {
			$("#dobParagraph").hide();
		}
	}

	function PhoneCheck() {
		var Phone = $('#phone').val();
		if (Phone.length == 0) {
			$("#numberParagraph").show();
			$("#numberParagraph").html("*Please fill the Phone no.");
			$("#numberParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		if (isNaN(Phone)) {
			$("#numberParagraph").show();
			$("#numberParagraph").html("*Phone number not contains string");
			$("#numberParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		if (Phone.length != 10) {
			$("#numberParagraph").show();
			$("#numberParagraph").html("*Length must be 10");
			$("#numberParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		else {
			$("#numberParagraph").hide();
		}
	}

	function EmailCheck() {
		var email = $('#email').val();
		if (email.length == 0) {
			$("#emailParagraph").show();
			$("#emailParagraph").html("*Please fill the Email address");
			$("#emailParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		var atSymbol = "@";
		var dotSymbol = ".";
		if (email.match(atSymbol) && email.match(dotSymbol)) {
			$("#emailParagraph").show();
			$("#emailParagraph").html("*Frequent Email");
			$("#emailParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		if (email.length > 256) {
			$("#emailParagraph").show();
			$("#emailParagraph").html("*Length must be less than 256");
			$("#emailParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		else {
			$("#emailParagraph").show();
			$("#emailParagraph").html("*Please fill the Email address");
			$("#emailParagraph").css("color", "red");
			user_err = false;
			return false;
		}
	}

	function CheckDept() {
		var dept = $('#dept').val();
		if (dept == null) {
			$("#deptParagraph").show();
			$("#deptParagraph").html("*please select the Department");
			$("#deptParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		else {
			$("#deptParagraph").hide();
		}
	}

	function CheckAddress() {
		var adr = $('#addr').val();
		if (adr.length == 0) {
			$("#addressParagraph").show();
			$("#addressParagraph").html("*Please fill the Address");
			$("#addressParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		else {
			$("#addressParagraph").hide();
		}
	}

	function CheckZip() {
		var zip = $('#zip').val();
		if (zip.length == 0) {
			$("#zipParagraph").show();
			$("#zipParagraph").html("*Please fill the Zip code");
			$("#zipParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		if (isNaN(zip)) {
			$("#zipParagraph").show();
			$("#zipParagraph").html("*Zip code not contains string");
			$("#zipParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		if (zip.length != 6) {
			$("#zipParagraph").show();
			$("#zipParagraph").html("**Length must be 6");
			$("#zipParagraph").css("color", "red");
			user_err = false;
			return false;
		}
		else {
			$("#zipParagraph").hide();
		}
	}

});
