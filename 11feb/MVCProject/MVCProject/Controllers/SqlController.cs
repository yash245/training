﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeeDataAccess;
namespace MVCProject.Controllers
{
    public class SqlController : Controller
    {
        public ActionResult Index()
        {
         return View();
        }
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult Delete(int id)
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                EmployeeDetail empTable = entity.EmployeeDetails.FirstOrDefault(emp => emp.empId == id);
                entity.EmployeeDetails.Remove(empTable);
                entity.SaveChanges();
                return RedirectToAction("Details");
            }
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                EmployeeDetail empTable = entity.EmployeeDetails.FirstOrDefault(emp => emp.empId == id);

                return View(empTable);
            }
        }
        [HttpPost]
        public ActionResult Edit(FormCollection form)
        {
            
            int id = int.Parse(form["empId"]);
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                EmployeeDetail emp = entity.EmployeeDetails.FirstOrDefault(employee => employee.empId == id );
                emp.empName = form["empName"];
                emp.dob = form["dob"];
                emp.city = form["city"];
                emp.addr = form["addr"];
                emp.dept = form["dept"];
                emp.email = form["email"];
                emp.state = form["state"];
                emp.phone = form["phone"];
                emp.joindate = form["joindate"];
                emp.zip = form["zip"];
                entity.SaveChanges();
                Response.Write("Data Saved successfully");
                return RedirectToAction("Details");
            }
        }
        [HttpPost]
        public ActionResult Register(EmployeeDetail emp)
        {
            //EmployeeDetail emp = new EmployeeDetail();
            //emp.empName = form["empName"];
            //emp.dob = form["dob"];
            //emp.city = form["city"];
            //emp.addr = form["addr"];
            //emp.dept = form["dept"];
            //emp.email = form["email"];
            //emp.state = form["state"];
            //emp.phone = form["phone"];
            //emp.joindate = form["joindate"];
            //emp.zip = form["zip"];

            using (EmployeeEntities entity = new EmployeeEntities())
            {
                EmployeeDetail empTable = entity.EmployeeDetails.Add(emp);
                entity.SaveChanges();
                Response.Write("Data Saved successfully");
                return RedirectToAction("Details");
            }
        }
        [HttpGet]
        public ActionResult Details()
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                List<EmployeeDetail> empTable = entity.EmployeeDetails.ToList();

                return View(empTable);
            }
        }
    }
}