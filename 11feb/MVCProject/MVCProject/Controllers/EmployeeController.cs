﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCProject.Models;
using EmployeeDataAccess;
namespace MVCProject.Controllers
{
    public class EmployeeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult Delete(int id)
        {
           using (EmployeeEntities entity = new EmployeeEntities())
            {
                EmployeeDetail empTable = entity.EmployeeDetails.FirstOrDefault(emp => emp.empId == id);
                entity.EmployeeDetails.Remove(empTable);
                entity.SaveChanges();
                return RedirectToAction("Details");
            }
       }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                EmployeeDetail empTable = entity.EmployeeDetails.FirstOrDefault(emp => emp.empId == id);
                Employee employee = new Employee();
                employee.empName = empTable.empName;
                employee.dob = empTable.dob;
                City.TryParse(empTable.city,out City city);
                employee.city = city;
                employee.addr = empTable.addr;
                Department.TryParse(empTable.city, out Department dept);
                employee.dept = dept;
                employee.email = empTable.email;
                State.TryParse(empTable.city, out State state);
                employee.state = state;
                employee.phone = empTable.phone;
                employee.joindate = empTable.joindate;
                employee.zip = empTable.zip;
                return View(employee);
            }
        }
        [HttpPost]
        public ActionResult Edit(int id, Employee employee)
        {
        if (ModelState.IsValid)
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                EmployeeDetail emp = entity.EmployeeDetails.FirstOrDefault(employees => employees.empId == id);
                emp.empName = employee.empName;
                emp.dob = employee.dob;
                emp.city = (employee.city).ToString();
                emp.addr = employee.addr;
                emp.dept = (employee.dept).ToString();
                emp.email = employee.email;
                emp.state = (employee.state).ToString();
                emp.phone = employee.phone;
                emp.joindate = employee.joindate;
                emp.zip = employee.zip;
                entity.SaveChanges();
                Response.Write("Data Saved successfully");
                return RedirectToAction("Details");
            }
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult Register(Employee employee)
        {
            if (ModelState.IsValid)
            {
                using (EmployeeEntities entity = new EmployeeEntities())
                {
                    EmployeeDetail emp = new EmployeeDetail();
                    emp.empName = employee.empName;
                    emp.dob = employee.dob;
                    emp.city = (employee.city).ToString();
                    emp.addr = employee.addr;
                    emp.dept = (employee.dept).ToString();
                    emp.email = employee.email;
                    emp.state = (employee.state).ToString();
                    emp.phone = employee.phone;
                    emp.joindate = employee.joindate;
                    emp.zip = employee.zip;
                    EmployeeDetail empTable = entity.EmployeeDetails.Add(emp);
                    entity.SaveChanges();
                    Response.Write("Data Saved successfully");
                    return RedirectToAction("Details");
                }
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Details()
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                List<EmployeeDetail> empTable = entity.EmployeeDetails.ToList();

                return View(empTable);
            }
        }
        public ActionResult Grid()
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                List<EmployeeDetail> empTable = entity.EmployeeDetails.ToList();

                return View(empTable);
            }
        }
        public ActionResult WebGrid()
        {
            using (EmployeeEntities entity = new EmployeeEntities())
            {
                List<EmployeeDetail> empTable = entity.EmployeeDetails.ToList();

                return View(empTable);
            }
        }

    }
}