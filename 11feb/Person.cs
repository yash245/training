﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vehicleUpdatoion
{
    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public void AddPerson()
        {
            List<Person> listOfPerson = new List<Person>();
            listOfPerson.Add(new Person {Name = "yash",Age= 22 });
            listOfPerson.Add(new Person { Name = "ragu", Age = 88 });
            listOfPerson.Add(new Person { Name = "harsh", Age = 70 });
            listOfPerson.Add(new Person { Name = "yashika", Age = 62 });
            listOfPerson.Add(new Person { Name = "budaa", Age = 92 });
            SeniorCitizen(listOfPerson);
        }
        public void SeniorCitizen(List<Person> senior)
        {
            foreach (var citizen in senior)
            {
                if (citizen.Age > 60)
                {
                    Console.WriteLine("Name : " +citizen.Name +"  Age : " +citizen.Age);
                }
            }
        }
    }
}
