﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class AnotherClass
    {
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public string Department { get; set; }
    }
}
