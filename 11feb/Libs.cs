﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public interface PrintFunc
    {
        void Print(object output);
        void Println(object output);
    }
    public class Libs : PrintFunc
    {
        public void Print(object output)
        {

            Console.Write(output);
            // throw new NotImplementedException();
        }
        public void Println(object output)
        {
            Console.WriteLine(output);//throw new NotImplementedException();
        }

        public static void Main(String[] args)
        {
            ChangeName ch = new ChangeName();
            string request = null;
            do
            {
                Console.WriteLine("select operation by your choice  \n 1.Add user \n 2.Dispaly \n 3.OrderBy \n 4.Skip \n 5.SkipWhile\n 6.ThenBydecending \n 7.CheckRecord \n 8.Take");
                Console.WriteLine("**********enter your choice***********");
                switch (Console.ReadLine())
                {
                    case "1":
                        request = null;
                        Console.WriteLine("enter the user name");
                        string name = Console.ReadLine();
                        Console.WriteLine("enter the user mobile number");
                        string mobile = Console.ReadLine();
                        Console.WriteLine("enter the user departname");
                        string department = Console.ReadLine();
                        Console.WriteLine(ch.UpdateName(name, mobile, department));

                        Console.WriteLine("Do you want to continue??? \n press any key to continue or n to stop the execution");
                        request = Console.ReadLine();
                        break;
                    case "2":
                        ch.Display();
                        break;
                    case "3":
                        ch.OrderBy();
                        break;
                    case "4":
                        ch.Skip();
                        break;
                    case "5":
                        ch.SkipWhile();
                        break;
                    case "6":
                        ch.ThenBy();
                        break;
                    case "7":
                        ch.CheckRecord();
                        break;
                    case "8":
                        ch.Take();
                        break;
                    default:
                        Console.WriteLine("enter a valid input");
                        break;

                }
            }
            while (request != "n");
            //string input = Console.ReadLine();
            //Libs libObj = new Libs();
            //libObj.Println(input);
            //libObj.Print(input);

            //Console.ReadKey();
        }

    }
}
