﻿using System;
using System.IO;
using System.Text;

namespace Train
{
    class FileHandling
    {
        public void CreateFile()
        {
            string filePath = @"C:\Users\yasha\Desktop\test\test.txt";
            try
            {

                // FileStream f = File.Create(filePath) ;
                if (File.Exists(filePath))
                { File.Delete(filePath); }
                FileStream fStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                Console.WriteLine("enter any input to write on file");
                string userInput = Console.ReadLine() ;
                byte[] b = Encoding.ASCII.GetBytes(userInput);
                fStream.Write(b);
                Console.WriteLine("fileLength : " + fStream.Length);
                fStream.Position = 100;
                
                // Read a text file line by line. 

                string[] lines = File.ReadAllLines(filePath);
                foreach (string line in lines)
                    Console.WriteLine(line);
                string append = Console.ReadLine();
                File.AppendAllText(filePath,append);
               
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("file not created");
            }
            finally
            {
                Console.WriteLine("This is from finally block");
                FileStream fStream = new FileStream(filePath, FileMode.OpenOrCreate);
                fStream.Close();
                Console.WriteLine("File is closed");

            }
        }
    }
}
