﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace vehicleUpdatoion
{
    class EnumMonth
    {
        public enum Months { January, February, March, April, May, June, July, August, September, October, November, December };
        public void AddMonth()
        {
            Dictionary<Months, int> daysOfMonth = new Dictionary<Months, int>();
            daysOfMonth.Add(Months.January, 31);
            daysOfMonth.Add(Months.February, 28);
            daysOfMonth.Add(Months.March, 31);
            daysOfMonth.Add(Months.April, 30);
            daysOfMonth.Add(Months.May, 31);
            daysOfMonth.Add(Months.June, 30);
            daysOfMonth.Add(Months.July, 31);
            daysOfMonth.Add(Months.August, 31);
            daysOfMonth.Add(Months.September, 30);
            daysOfMonth.Add(Months.October, 31);
            daysOfMonth.Add(Months.November, 30);
            daysOfMonth.Add(Months.December, 31);
            Console.WriteLine("enter the month would you want to find");
            string month = Console.ReadLine();
            FindingMonths(daysOfMonth, month);
        }
        public void FindingMonths(Dictionary<Months,int> dic,string ms)
        {
            int flag = 0;
            foreach (KeyValuePair<Months, int> month in dic)
            {
                if (month.Key.ToString() == ms)
                {
                    Console.WriteLine(ms + " having " + month.Value + " days");
                    break;
                }
                else
                    flag = 1;
            }
            if (flag == 1)
            {
                Console.WriteLine("please enter the valid month");
            }
        }
    }

}
