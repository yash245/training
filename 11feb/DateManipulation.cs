﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extension
{
    public static class DateManipulation
    {
        public static string IndStyle()
        { return DateTime.Now.ToString("dd/MM/yyyy"); }
        public static Boolean getdate(string date)
        {
            if (DateTime.TryParse(date, out DateTime dates))
                return true;
            else
                return false;
        }
        public static string IndStyle(this string date)
        {
            if (getdate(date))
            {
                DateTime d = Convert.ToDateTime(date);
                return d.ToString("dd/MM/yyyy HH:mm");

            }
            else
                return "enter a valid date";
         
        }
        public static string DotStyle(this string date)
        {
            Func<string> getdate = delegate () {
                DateTime d = Convert.ToDateTime(date);
                return d.ToString("dd.MM.yyyy HH.mm");
            };
            return date ?? getdate();
           
        }

        public static string SecondDotStyle(this string date)
        {
            Func<string> getdate = delegate () {
                DateTime d = Convert.ToDateTime(date);
                return d.ToString("dd.MM.yyyy HH:mm:ss");
            };
            return date ?? getdate();
           
        }
        public static string SecondSlashStyle(this string date)
        {
            Func<string> getdate = delegate () {
                DateTime d = Convert.ToDateTime(date);
                return d.ToString("dd/MM/yyyy HH:mm:ss");
            };
            return date ?? getdate();
         
        }

        
    }
    public class StringFunction
    {
        public static int? ParseIntReturnNull(string number)
        {
            return int.TryParse(number, out int num) ? num : (int?)null;
        }
        public static int? ParseIntReturnNull(object number)
        {
            string input = (string)number;
            return int.TryParse(input, out int num) ? num : (int?)null;

        }
        public static int ParseInt(string number)
        {
            return int.TryParse(number, out int num) ? num : default;
          
        }
        public static int? ParseInt(object number)
        {
            string input = number.ToString();
            int num;
            return int.TryParse(input, out num) ? num : default;
            //if (int.TryParse(input, out int num))
            //    return num;
            //else
            //    return default;

        }

    }
}
