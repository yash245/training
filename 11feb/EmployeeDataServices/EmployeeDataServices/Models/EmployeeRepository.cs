﻿using EmployeeData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeDataServices.Models
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private List<EmployeeRecord> _employeeList;
        public EmployeeRepository()
        {
            _employeeList = new List<EmployeeRecord>()
            {
                new EmployeeRecord(){ EmployeeName="yash",Id=1,City="Delhi" },
                new EmployeeRecord(){ EmployeeName="vishal",Id=3,City="Delhi" },
                new EmployeeRecord(){ EmployeeName="vinay",Id=2,City="Delhi" }
            };

        }
        public EmployeeDetail ChangeEmployee(Employee employee)
        {
            EmployeeDetail emp = new EmployeeDetail();
            emp.EmployeeName = employee.EmployeeName;
            emp.DOB = employee.DOB;
            emp.City = employee.City;
            emp.Address = employee.Address;
            emp.DepartmentName = employee.DepartmentName;
            emp.Email = employee.Email;
            emp.State = employee.State;
            emp.Phone = employee.Phone;
            emp.JoiningDate = employee.JoiningDate;
            emp.Zip = employee.Zip;
            return emp;
        }
        public EmployeeRecord GetEmployee(int id)
        {
            return _employeeList.Find(e => e.Id == id);
        }

    }
}
