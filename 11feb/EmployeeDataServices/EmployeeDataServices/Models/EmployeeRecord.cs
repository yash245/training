﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeDataServices.Models
{
    public class EmployeeRecord
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public string City { get; set; }

    }
}
