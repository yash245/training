﻿using EmployeeData;
namespace EmployeeDataServices.Models
{
    public interface IEmployeeRepository
    {
        EmployeeRecord GetEmployee(int id);
        EmployeeDetail ChangeEmployee(Employee employee);
    }
}