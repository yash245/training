﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EmployeeData;
using EmployeeDataServices.Models;

namespace EmployeeDataServices.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly DataContext _context;
        private readonly IEmployeeRepository _employeeRepository;
        public EmployeesController(DataContext context, IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
            _context = context;
        }

        // GET: Employees
        public async Task<IActionResult> Index()
        {
            if (ModelState.IsValid)
            {
                var emp = _context.Employees.Include(e => e.Department).Select(e => new EmployeeDetail
                {
                    Id = e.Id,
                    EmployeeName = e.EmployeeName,
                    DOB = e.DOB,
                    Phone = e.Phone,
                    Email = e.Email,
                    Address = e.Address,
                    State = e.State,
                    City = e.City,
                    Zip = e.Zip,
                    DepartmentName = e.DepartmentName
                });
                return View(await emp.ToListAsync());
            }
            else
            { return RedirectToRoute("Index"); }
        }

        // GET: Employees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                 .FirstOrDefaultAsync(m => m.Id == id);
            if (employee == null)
            {
                return RedirectToRoute("Create");
            }
            EmployeeDetail emp = _employeeRepository.ChangeEmployee(employee);
            return View(emp);
        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            ViewBag.State = new SelectList(_context.States, "StateName", "StateName");
            List<Department> departments = _context.Departments.ToList();
            ViewBag.DepartmentName = new SelectList(departments, "DepartmentName", "DepartmentName");
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Employee employee)
        {

            employee.DepartmentId = _context.Departments.Where(d => d.DepartmentName.Equals(employee.DepartmentName)).Select(f => f.Id).SingleOrDefault();
            if (ModelState.IsValid)
            {
                _context.Add(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            EmployeeDetail emp = _employeeRepository.ChangeEmployee(employee);
            ViewBag.State = new SelectList(_context.States, "StateName", "StateName");
            List<Department> departments = _context.Departments.ToList();
            ViewBag.DepartmentName = new SelectList(departments, "DepartmentName", "DepartmentName");
            return View(emp);
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            EmployeeDetail emp = _employeeRepository.ChangeEmployee(employee);
            ViewBag.City = new SelectList(_context.Cities.Where(x => x.State.StateName == employee.State), "CityName", "CityName");
            ViewBag.State = new SelectList(_context.States, "StateName", "StateName");
            List<Department> departments = _context.Departments.ToList();
            ViewBag.DepartmentName = new SelectList(departments, "DepartmentName", "DepartmentName");
            return View(emp);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Employee employee)
        {
            employee.DepartmentId = _context.Departments.Where(d => d.DepartmentName.Equals(employee.DepartmentName)).Select(f => f.Id).SingleOrDefault();
            if (id != employee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            List<Department> departments = _context.Departments.ToList();
            ViewBag.DepartmentName = new SelectList(departments, "DepartmentName", "DepartmentName");
            return View(employee);
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }
    }
}
