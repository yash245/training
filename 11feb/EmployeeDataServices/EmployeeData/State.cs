﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeData
{
    public class State
    {
        public string StateName { get; set; }
        public int Id { get; set; }
        public List<City> City { get; set; }
    }
}
