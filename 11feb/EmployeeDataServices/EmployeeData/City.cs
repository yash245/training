﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeData
{
    public class City
    {
        public string CityName { get; set; }
        public int Id { get; set; }
        public State State { get; set; }
        public int StateId { get; set; }
    }
}
