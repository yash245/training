﻿using Microsoft.EntityFrameworkCore;
using System;

namespace EmployeeData
{
    public class DataContext : DbContext
    {
        private string _connectionString;
        public DataContext(string connectionString)
        {
            _connectionString = connectionString;
        }
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }
        
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer(_connectionString);
            }
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Employee>(e =>
            {
                e.ToTable("Employees");
                e.HasKey(x => x.Id);
            });
            base.OnModelCreating(builder);

            builder.Entity<Department>(d =>
            {
                d.ToTable("Departments");
                d.HasKey(x => x.Id);
            });

        }

    }
}
