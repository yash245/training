﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeData
{
    public class Department
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public virtual List<Employee> Employees { get; set; }

    }
}
