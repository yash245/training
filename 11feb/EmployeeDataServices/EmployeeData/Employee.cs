﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeData
{
    public class Employee
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public DateTime DOB { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public DateTime JoiningDate { get; set; }
        public Department Department { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
    }
}
