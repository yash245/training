﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Windows.Documents;
using System.Web.UI.MobileControls;

namespace ConsoleApp2
{
    public class ChangeName
    {
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public string Department { get; set; }
        int count;
        IList<ChangeName> updateNaam = new List<ChangeName>();
        public void Join()
        {
            updateNaam.Add(new ChangeName { Name="ayush",MobileNumber="89384-293049",Department="xamarin"});
            updateNaam.Add(new ChangeName { Name="prabhat",MobileNumber="89384-293049",Department="xamarin"});
            updateNaam.Add(new ChangeName { Name="ashish",MobileNumber="89384-293049",Department="xamarin"});
            updateNaam.Add(new ChangeName { Name="yash",MobileNumber="89384-293049",Department="xamarin"});
            IList<AnotherClass> anotherList = new List<AnotherClass>();
            anotherList.Add(new AnotherClass { Name="yash",MobileNumber="90490-8088",Department=".net"});
            anotherList.Add(new AnotherClass { Name="akash",MobileNumber="90490-8088",Department=".net"});
            anotherList.Add(new AnotherClass { Name="vivek",MobileNumber="90490-8088",Department=".net"});
            anotherList.Add(new AnotherClass { Name="vishal",MobileNumber="90490-8088",Department=".net"});
            var resultList = from i in updateNaam
                             join b in anotherList
                             on i.Name equals b.Name
                             select new
                             {
                                 i.Name,
                                 b.Department
                             };
            foreach (var item in resultList)
            { Console.WriteLine(item.Name); 
             Console.WriteLine(item.Department); }

        }
        public void Union()
        {
            updateNaam.Add(new ChangeName { Name = "ayush", MobileNumber = "89384-293049", Department = "xamarin" });
            updateNaam.Add(new ChangeName { Name = "prabhat", MobileNumber = "89384-293049", Department = "xamarin" });
            updateNaam.Add(new ChangeName { Name = "ashish", MobileNumber = "89384-293049", Department = "xamarin" });
            updateNaam.Add(new ChangeName { Name = "yash", MobileNumber = "89384-293049", Department = "xamarin" });
            IList<AnotherClass> anotherList = new List<AnotherClass>();
            anotherList.Add(new AnotherClass { Name = "yash", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "yash", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "yash", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "yash", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "akash", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "vivek", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "vishal", MobileNumber = "90490-8088", Department = ".net" });
            var resultList = from i in updateNaam
                             from b in anotherList

                             select new { i.Name, b.Department, i.MobileNumber };

            foreach (var item in resultList)
            {
                Console.WriteLine(item.Name+"\t"+item.Department+"\t"+item.MobileNumber);
               
            }

        }

        public void LeftJoin()
        {
            updateNaam.Add(new ChangeName { Name = "ayush", MobileNumber = "89384-293049", Department = "xamarin" });
            updateNaam.Add(new ChangeName { Name = "prabhat", MobileNumber = "89384-293049", Department = "xamarin" });
            updateNaam.Add(new ChangeName { Name = "ashish", MobileNumber = "89384-293049", Department = "xamarin" });
            updateNaam.Add(new ChangeName { Name = "yash", MobileNumber = "89384-293049", Department = "xamarin" });
            IList<AnotherClass> anotherList = new List<AnotherClass>();
            anotherList.Add(new AnotherClass { Name = "yash", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "akash", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "vivek", MobileNumber = "90490-8088", Department = ".net" });
            anotherList.Add(new AnotherClass { Name = "vishal", MobileNumber = "90490-8088", Department = ".net" });
            var resultList = from i in updateNaam
                             join b in anotherList
                             on i.Name equals b.Name into department
                             from employee in department.DefaultIfEmpty()
                            
                             select new
                             {
                                 employee.Name,
                                 employee.Department,
                                 employee.MobileNumber
                             };
            foreach (var item in resultList)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.Department);
            }

        }


        public void Display()
        {
            foreach (var item in updateNaam)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.MobileNumber);
            }
        }

        public string UpdateName(object nameInput, string mobile, string depart)
        {
            count = updateNaam.Where(x => x.Name.Contains(nameInput.ToString())).Count();
            if (count == 1)
            {
                string output = string.Concat(nameInput.ToString(), "_", count.ToString());
                updateNaam.Add(new ChangeName { Name = output, MobileNumber = mobile, Department = depart });
            }

            else if (count > 1)
            {
                string output = string.Concat(nameInput.ToString(), "_", count.ToString());
                updateNaam.Add(new ChangeName { Name = output, MobileNumber = mobile, Department = depart });
                count++;

            }
            else
                updateNaam.Add(new ChangeName { Name = (string)nameInput, MobileNumber = mobile, Department = depart });
            count = 0;
            return "record added successfully";

        }
        public void CheckRecord()
        {
            Console.WriteLine("enter your mobile number");
            string mobile = Console.ReadLine();
            var record = updateNaam.Where(x => x.MobileNumber.Equals(mobile)).ToList();
            foreach (var item in record)
            {
                Console.WriteLine(item.Name);
            }

        }
        public void Skip()
        {
            Console.WriteLine("enter your department name");
            var record = updateNaam.Where(x => x.Department.Equals(Console.ReadLine())).Skip(2).ToList();
            foreach (var item in record)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.Department);
                Console.WriteLine(item.MobileNumber);
            }
        }
        public void SkipWhile()
        {
            Console.WriteLine("enter the department you want to skip");
            string depart = Console.ReadLine();
            var record = updateNaam.OrderBy(x => x.Department).SkipWhile(x => x.Department.Equals(depart)).ToList();
            foreach (var item in record)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.Department);
                Console.WriteLine(item.MobileNumber);
            }
        }
        public void Take()
        {
            Console.WriteLine("enter your department name");
            var record = updateNaam.Where(x => x.Department.Equals(Console.ReadLine())).Take(2).ToList();
            foreach (var item in record)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.Department);
                Console.WriteLine(item.MobileNumber);
            }

        }
        public void OrderBy()
        {
            var record = updateNaam.OrderBy(x => x.Name).ToList();
            foreach (var item in record)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.Department);
                Console.WriteLine(item.MobileNumber);
            }

        }
        public void ThenBy()
        {
            var record = updateNaam.OrderByDescending(x => x.Name).ThenByDescending(y => y.Department).ToList();
            foreach (var item in record)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.Department);
                Console.WriteLine(item.MobileNumber);
            }

        }
        public void GroupBy()
        {
            var record = updateNaam.GroupBy(x => x.Department).ToList();
            foreach (var group in record)
            {
                Console.WriteLine(group.Key);
                foreach (ChangeName item in group)
                {
                    Console.WriteLine(item.Name);

                }
            }

        }

    }
}
