
	$(document).ready(function(){
	$("#nameParagraph").hide();
	$("#dobParagraph").hide();
	$("#numberParagraph").hide();
	$("#emailParagraph").hide();
	$("#deptParagraph").hide();
	$("#addressParagraph").hide();
	$("#stateParagraph").hide();
	$("#cityParagraph").hide();
	$("#zipParagraph").hide(); 
	$("#table").hide();

	var user_err=true;
	var citiesByState = {
	Chennai: ["Chennai"],
	Maharashtra: ["Mumbai","Pune","Nagpur"],
	Delhi: ["New Delhi","Old Delhi"]
	}
	 $("#state").change(function(){
	  var state = $('#state').val();
	   if (state==null) {
		   $("#stateParagraph").show();
		  $('#stateParagraph').text('State cant be Empty');
		  $("#stateParagraph").css("color", "red"); 
		}
		else if(state.length==0) {
			$('#city').text( "<option>Select City</option>");
			}
		else{
		var citiesOptions = "";
		for(cityId in citiesByState[state]) {
		citiesOptions+="<option>"+citiesByState[state][cityId]+"</option>";
		 }
		 $('#city').html(citiesOptions);
			  $('#stateParagraph').hide();
		 }
	  });
			
	$('#getData').click(function (){
		$('#Register').hide();
		$('#table').show();
       $.ajax({  
           type: "GET",  
           url: "https://localhost:44320/api/employee",  
           contentType: "application/json; charset=utf-8",  
           dataType: "json",  
           success: function (data) {  
               var DIV = '';  
               $.each(data, function (i, item) {  
                   var rows = "<tr>" +  
                       "<td >" + item.empName + "</td>" +  
                       "<td >" + item.dob + "</td>" +  
                       "<td '>" + item.phone + "</td>" +  
                       "<td >" + item.email + "</td>" +  
						"<td>" + item.dept + "</td>" +
						"<td>" + item.addr + "</td>" +
						"<td>" + item.state + "</td>" +
						"<td>" + item.city + "</td>" +
						"<td >" + item.zip + "</td>" +
						"<td>" + item.joindate + "</td>" +
						 '<td><a href="EditDetailsJQ.html?id='+item.empId +'"><button type="button" onclick="EditDetails('+item.empId+')" id="edit" class="btn btn-primary"> Edit</button> </td>'+
						 '<td><input type="button" id="delete" class="btn btn" onclick="Delete('+item.empId+')" value="Delete"/></td>'+
                       "</tr>";  
                   $('#tbl').append(rows);  
               });  
           }
       }); 	
	});
		

	$('#name').keyup(function(){
	CheckName();
	});
	$('#dob').change(function(){
	DobCheck();
	});
	$('#phone').keyup(function(){
	PhoneCheck();
	});
	$('#email').keyup(function(){
	EmailCheck();
	});
	$('#department').change(function(){
	CheckDept();
	});
	$('#address').keyup(function(){
	CheckAddress();
	});
	$('#zip').keyup(function(){
	CheckZip();
	});
	$('#editDetails').click(function(){
		var user=$('#name').val();
	if(user.length== 0){
	$("#nameParagraph").show();
	$("#nameParagraph").html("*please fill the Name");
	$("#nameParagraph").css("color","red");
	
	}
	var adr=$('#address').val();
	if(adr.length== 0){
	$("#addressParagraph").show();
	$("#addressParagraph").html("*Please fill the Address");
	$("#addressParagraph").css("color","red");
	
	}
	var dept=$('#department').val();
	if (dept ==null){
	$("#deptParagraph").show();
	$("#deptParagraph").html("*please select the Department");
	$("#deptParagraph").css("color","red");
	}
	var zip=$('#zip').val();
	if(zip.length== 0){
	$("#zipParagraph").show();
	$("#zipParagraph").html("*Please fill the Zip code");
	$("#zipParagraph").css("color","red");
	
	}
	var dob=$('#dob').val();
	if(dob.length== 0){
	$("#dobParagraph").show();
	$("#dobParagraph").html("**Please fill the DOB");
	$("#dobParagraph").css("color","red");
	
	}
    var state = $('#state').val();
    if (state==null) {
	$("#stateParagraph").show();
	  $('#stateParagraph').text('State cant be Empty');
	  $("#stateParagraph").css("color", "red"); 
	}
	var Phone=$('#phone').val();
	if(Phone.length== 0){
	$("#numberParagraph").show();
	$("#numberParagraph").html("*Please fill the Phone no.");
	$("#numberParagraph").css("color","red");
	
	}
	var email=$('#email').val();
	if(email.length== 0){
	$("#emailParagraph").show();
	$("#emailParagraph").html("*Please fill the Email address");
	$("#emailParagraph").css("color","red");
	
	}
	if($('#name').val()==""||$('#dob').val()==""||$('#email').val()==""||$('#number').val()==""||$('#department').val()==null||$('#state').val()==null||$('#zip').val()==""||$('#address').val()=="")
	{
		return false;
	}
	else{
             $.ajax({
                 type: "PUT",
                 url: "https://localhost:44320/api/employee"+id,
                 contentType: "application/json;charset=utf-8",
                 data: JSON.stringify({
                     empName: $("#name").val(),
                     dob: $("#dob").val(),
                     phone: $("#phone").val(),
                     email: $("#email").val(),
                     dept: $("#department").val(),
                     addr: $("#address").val(),
                     state: $("#state").val(),
                     city: $("#city").val(),
                     zip: $("#zip").val(),
                     joindate: $("#joinDate").val()
                 }),
				 success: function () {
                     alert('data Added Successfully');}
         });
	$('#Register').hide();
		$('#table').show();
       $.ajax({  
           type: "GET",  
           url: "https://localhost:44320/api/employee",  
           contentType: "application/json; charset=utf-8",  
           dataType: "json",  
           success: function (data) {  
               var DIV = '';  
               $.each(data, function (i, item) {  
                   var rows = "<tr>" +  
                       "<td >" + item.empName + "</td>" +  
                       "<td >" + item.dob + "</td>" +  
                       "<td '>" + item.phone + "</td>" +  
                       "<td >" + item.email + "</td>" +  
						"<td>" + item.dept + "</td>" +
						"<td>" + item.addr + "</td>" +
						"<td>" + item.state + "</td>" +
						"<td>" + item.city + "</td>" +
						"<td >" + item.zip + "</td>" +
						"<td>" + item.joindate + "</td>" +
						  '<td><a href="EditDetailsJQ.html?id='+item.empId +'"><button type="button" id="edit" class="btn btn-primary"> Edit</button> </td>'+
                       "</tr>";  
                   $('#tbl').append(rows);  
               });  
           }
       });    
	}
	});
	function CheckName(){
	var user=$('#name').val();
	if(user.length== 0){
	$("#nameParagraph").show();
	$("#nameParagraph").html("*please fill the Name");
	$("#nameParagraph").css("color","red");
	user_err=false;
	return false;
	}
	else{
	$("#nameParagraph").hide();
	}
	}
	
	function DobCheck(){
	var dob=$('#dob').val();
	if(dob.length== 0){
	$("#dobParagraph").show();
	$("#dobParagraph").html("**Please fill the DOB");
	$("#dobParagraph").css("color","red");
	user_err=false;
	return false;
	}
	else{
	$("#dobParagraph").hide();
	}
	}
	
	function PhoneCheck(){
	var Phone=$('#phone').val();
	if(Phone.length== 0){
	$("#numberParagraph").show();
	$("#numberParagraph").html("*Please fill the Phone no.");
	$("#numberParagraph").css("color","red");
	user_err=false;
	return false;
	}
	if(isNaN(Phone))
		{
	$("#numberParagraph").show();
	$("#numberParagraph").html("*Phone number not contains string");
	$("#numberParagraph").css("color","red");
	user_err=false;
	return false;
	}	
	if(Phone.length!=10){
	$("#numberParagraph").show();
	$("#numberParagraph").html("*Length must be 10");
	$("#numberParagraph").css("color","red");
	user_err=false;
	return false;
	}
	else{
	$("#numberParagraph").hide();
	}
	}
	
	function EmailCheck(){
	var email=$('#email').val();
	if(email.length== 0){
	$("#emailParagraph").show();
	$("#emailParagraph").html("*Please fill the Email address");
	$("#emailParagraph").css("color","red");
	user_err=false;
	return false;
	}
	var atSymbol = "@";
    var dotSymbol=".";
	if(email.match(atSymbol) && email.match(dotSymbol))
	{    
	$("#emailParagraph").show();
	$("#emailParagraph").html("*Frequent Email");
	$("#emailParagraph").css("color","red");
	user_err=false;
	return false;
	}
	if(email.length>256){
	$("#emailParagraph").show();
	$("#emailParagraph").html("*Length must be less than 256");
	$("#emailParagraph").css("color","red");
	user_err=false;
	return false;
	}
	else
	{
	$("#emailParagraph").show();
	$("#emailParagraph").html("*Please fill the Email address");
	$("#emailParagraph").css("color","red");
	user_err=false;
	return false;
	}		
	}
	
	function CheckDept(){
	var dept=$('#department').val();
	if (dept == null){
	$("#deptParagraph").show();
	$("#deptParagraph").html("*please select the Department");
	$("#deptParagraph").css("color","red");
	user_err=false;
	return false;
	}
	else{
	$("#deptParagraph").hide();
	}
	}
	
	function CheckAddress(){
	var adr=$('#address').val();
	if(adr.length== 0){
	$("#addressParagraph").show();
	$("#addressParagraph").html("*Please fill the Address");
	$("#addressParagraph").css("color","red");
	user_err=false;
	return false;
	}
	else{
	$("#addressParagraph").hide();
	}
	}
	
	function CheckZip(){
	var zip=$('#zip').val();
	if(zip.length== 0){
	$("#zipParagraph").show();
	$("#zipParagraph").html("*Please fill the Zip code");
	$("#zipParagraph").css("color","red");
	user_err=false;
	return false;
	}
	if(isNaN(zip))
		{
	$("#zipParagraph").show();
	$("#zipParagraph").html("*Zip code not contains string");
	$("#zipParagraph").css("color","red");
	user_err=false;
	return false;
	}
	if(zip.length!=6){
	$("#zipParagraph").show();
	$("#zipParagraph").html("**Length must be 6");
	$("#zipParagraph").css("color","red");
	user_err=false;
	return false;
	}
	else{
	$("#zipParagraph").hide();
	}
	}
	
	});
	