function validateFunction()
{
	var name=document.forms["validate"]["name"].value;
	var dob=document.forms["validate"]["dob"].value;
	var joindate=document.forms["validate"]["joiningdate"].value;
	var state=document.forms["validate"]["state"].value;
	var zip=document.forms["validate"]["zip"].value;
	var city=document.forms["validate"]["city"].value;
	var number=document.forms["validate"]["number"].value;
	var email=document.forms["validate"]["email"].value;
	var dept=document.forms["validate"]["dept"].value;
	var address=document.forms["validate"]["address"].value;
	
	
	
	if(name=="")
	{
		document.getElementById("namep").innerHTML= "*name must required";
		document.getElementById("namep").style.color="red";
	}
	if(state=="")
	{
		document.getElementById("statep").innerHTML= "*state must required";
		document.getElementById("statep").style.color="red";
	}
	 if(email=="")
	{
		document.getElementById("emailp").innerHTML= "*email must required";
		document.getElementById("emailp").style.color="red";
	}
	if(dept=="")
	{
		document.getElementById("deptp").innerHTML= "*department must required";
		document.getElementById("deptp").style.color="red";
	}
	if(address=="")
	{
		document.getElementById("addressp").innerHTML= "*address must required";
		document.getElementById("addressp").style.color="red";
	}
	 
	 if(number=="")
	{
		document.getElementById("numberp").innerHTML= "*Phone Number must required";
		document.getElementById("numberp").style.color="red";
	}
    if(zip=="")
		{
		document.getElementById("zipp").innerHTML= "*zip code must required";
		document.getElementById("zipp").style.color="red";
		
	}		
	if(name==""||email==""||dept==""||address==""||number==""||zip==""||state=="")
	{
		return false;
	}
}
    function validateInput(value,id)
	{
		if(value=="")
		{
			document.getElementById(id).innerHTML="*Field can't be empty";
			document.getElementById(id).style.color="red";
		}
		else if(id=="numberp")
		{if(isNaN(value))
		 {
			document.getElementById(id).innerHTML= "*Not Valid!mobile number having string";
			document.getElementById(id).style.color="red";
		 }
		else if(value.length!=10)
		 {
			 document.getElementById(id).innerHTML= "*mobile number must of 10 digits";
			document.getElementById(id).style.color="red";
		 }
		  else
		 {
			 document.getElementById(id).innerHTML="";
		 }
		 }
		 else if(id=="zipp")
		 {
		if(isNaN(value))
		 {
			document.getElementById(id).innerHTML= "*Not Valid!Zip code having String ";
			document.getElementById(id).style.color="red";
		 }
		else if(value.length!=6)
	     {
		 document.getElementById(id).innerHTML= "*Zip Code must have 6 digits";
		 document.getElementById(id).style.color="red";
		 }
		 else
			 {
				 document.getElementById(id).innerHTML="";
			 }
	
		}
		else if(id=="emailp")
		{
		var atSymbol = "@";
		var dotSymbol=".";
		if(value.match(atSymbol) && value.match(dotSymbol))
		{    
			document.getElementById(id).innerHTML="*valid email";
			document.getElementById(id).style.color="red";
		}
		else
		{
		document.getElementById(id).innerHTML= "*You have entered a invalid email address!";
		document.getElementById(id).style.color="red";
		}		
		}	
		else
		{
			document.getElementById(id).innerHTML="";
		}
	}
	
	var citiesByState = {
	Chennai: ["Chennai"],
	Maharashtra: ["Mumbai","Pune","Nagpur"],
	Delhi: ["New Delhi","Old Delhi"]
	}
	function selectCity(value)
	 {
	if(value.length==0) 
	{document.getElementById("city").innerHTML = "<option></option>";}
	else {
	var citiesOptions = "";
	for(cityId in citiesByState[value]) {
	citiesOptions+="<option>"+citiesByState[value][cityId]+"</option>";
	}
	document.getElementById("city").innerHTML = citiesOptions;
	}
	}
