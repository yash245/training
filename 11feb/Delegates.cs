﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Extension;
using System.Threading.Tasks;


namespace LambdaTraining
{
    public class Delegates
    {

        public delegate int ChangeNum(int input, int number);

        public int Addition(int input, int number)
        {
            input = input + number;
            return input;
        }

        public int Multiplication(int input, int number)
        {
            input = input * number;
            return input;
        }

        public decimal Divide(int input, int number)
        {

            return input / number;
        }

        public static void Main(string[] args)
        {
            Delegates delObj = new Delegates();

            ChangeNum number = new ChangeNum(delObj.Addition);
            Console.WriteLine(number(23, 33));
            ChangeNum numb = new ChangeNum(delObj.Multiplication);
            Console.WriteLine(numb(23, 23));

            Func<int, int, decimal> div = delObj.Divide;
            Console.WriteLine(div(24, 23));

            Func<int, int, int> sum = (x, y) => x + y;
            Console.WriteLine(sum(2, 23));

            Func<int, int, int> add = delegate (int x, int y) { Delegates d = new Delegates(); return d.Addition(x, y); };
            Console.WriteLine(add(20, 20));
            //  Predicate<int> 
            Func<string> date = delegate () { return DateManipulation.IndStyle("12/22/2020  23:32"); };
            Console.WriteLine(date());

            object yash = 121;
            Console.WriteLine(StringFunction.ParseInt(yash));

            Console.ReadKey();
        }
    }
   
}