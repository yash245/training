﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListTraining
{
    class NumAddition
    {
        public static void Main(String[] args)
        {
            int ascii;
            int sum = 0;
            Console.WriteLine("Enter a String");
            string userString = Console.ReadLine();
            char[] arrayOfString = userString.ToCharArray();
            foreach (char ch in arrayOfString)
            {
                ascii = (int)ch;
                if (48 <= ascii || ascii >= 5)
                {
                    sum = sum + ascii;
                }
            }
            Console.WriteLine(sum);
            Console.ReadKey();
        }
    }
}
