﻿using System;
using System.Collections.Generic;

namespace Train
{
    class TryCatch
    {
        static void Main(string[] args)
        {
            try
            {
               // FileHandling fHandle = new FileHandling();
                //fHandle.CreateFile();
                Console.WriteLine("this is from try block");
                Console.WriteLine("Enter a dividend number");
                string number = Console.ReadLine();
                int dividend = int.Parse(number);
                Console.WriteLine("Enter the divisior number");
                string divis = Console.ReadLine();
                int divisor = int.Parse(divis);
                Console.WriteLine("quitoent is : " + dividend / divisor);
                Console.WriteLine("remender is : " + dividend % divisor);
                Console.WriteLine("enter your age is above then eighteen");
                string ages = Console.ReadLine();
                int age = int.Parse(ages);
                if (age < 18)
                    throw new InvalidAge(string.Format("please enter a valid age"));
     
                Console.WriteLine();
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("From catch block");
                Console.WriteLine("Please enter a valid divisior");
            }
            catch (ArithmeticException)
            {
                Console.WriteLine("From catch block");
                Console.WriteLine("Please enter a valid dividend and divisior");
            }
            catch (Exception)
            {
                Console.WriteLine("From catch block");
                Console.WriteLine("\n\n\nPlease enter a vaild input");
            }
            
            Console.ReadKey();
        }
    }
    class InvalidAge :Exception
    { public InvalidAge(string msg) : base(msg)
        {
            Console.WriteLine(msg);
        }
    }
}