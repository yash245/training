﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkProject.Models
{
    public class Student
    {
        [Key]
        public int StudentID { get; set; }
        public int StudentName { get; set; }
        public List<Course> Courses { get; set; }
    }
}