﻿namespace EntityFrameworkProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeeDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                        DepartmentTeam = c.String(),
                    })
                .PrimaryKey(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        EmployeeName = c.String(),
                        Department = c.String(),
                        Salary = c.Int(nullable: false),
                        Departments_DepartmentId = c.Int(),
                    })
                .PrimaryKey(t => t.EmployeeId)
                .ForeignKey("dbo.Departments", t => t.Departments_DepartmentId)
                .Index(t => t.Departments_DepartmentId);
            
            CreateTable(
                "dbo.EmployeeDetails",
                c => new
                    {
                        EmpId = c.Int(nullable: false, identity: true),
                        EmployeeName = c.String(),
                        Salary = c.Int(nullable: false),
                        Gender = c.String(),
                        Department_DepartmentId = c.Int(),
                    })
                .PrimaryKey(t => t.EmpId)
                .ForeignKey("dbo.Departments", t => t.Department_DepartmentId)
                .Index(t => t.Department_DepartmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeDetails", "Department_DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Employees", "Departments_DepartmentId", "dbo.Departments");
            DropIndex("dbo.EmployeeDetails", new[] { "Department_DepartmentId" });
            DropIndex("dbo.Employees", new[] { "Departments_DepartmentId" });
            DropTable("dbo.EmployeeDetails");
            DropTable("dbo.Employees");
            DropTable("dbo.Departments");
        }
    }
}
