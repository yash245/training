﻿namespace EntityFrameworkProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Student : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employees", "Departments_DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.EmployeeDetails", "Department_DepartmentId", "dbo.Departments");
            DropIndex("dbo.Employees", new[] { "Departments_DepartmentId" });
            DropIndex("dbo.EmployeeDetails", new[] { "Department_DepartmentId" });
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseId = c.Int(nullable: false, identity: true),
                        CourseName = c.String(),
                    })
                .PrimaryKey(t => t.CourseId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentID = c.Int(nullable: false, identity: true),
                        StudentName = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudentID);
            
            CreateTable(
                "dbo.StudentCourses",
                c => new
                    {
                        Student_StudentID = c.Int(nullable: false),
                        Course_CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Student_StudentID, t.Course_CourseId })
                .ForeignKey("dbo.Students", t => t.Student_StudentID, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.Course_CourseId, cascadeDelete: true)
                .Index(t => t.Student_StudentID)
                .Index(t => t.Course_CourseId);
            
            DropTable("dbo.Departments");
            DropTable("dbo.Employees");
            DropTable("dbo.EmployeeDetails");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmployeeDetails",
                c => new
                    {
                        EmpId = c.Int(nullable: false, identity: true),
                        EmployeeName = c.String(),
                        Salary = c.Int(nullable: false),
                        Gender = c.String(),
                        Department_DepartmentId = c.Int(),
                    })
                .PrimaryKey(t => t.EmpId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        EmployeeName = c.String(),
                        Department = c.String(),
                        Salary = c.Int(nullable: false),
                        Departments_DepartmentId = c.Int(),
                    })
                .PrimaryKey(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                        DepartmentTeam = c.String(),
                    })
                .PrimaryKey(t => t.DepartmentId);
            
            DropForeignKey("dbo.StudentCourses", "Course_CourseId", "dbo.Courses");
            DropForeignKey("dbo.StudentCourses", "Student_StudentID", "dbo.Students");
            DropIndex("dbo.StudentCourses", new[] { "Course_CourseId" });
            DropIndex("dbo.StudentCourses", new[] { "Student_StudentID" });
            DropTable("dbo.StudentCourses");
            DropTable("dbo.Students");
            DropTable("dbo.Courses");
            CreateIndex("dbo.EmployeeDetails", "Department_DepartmentId");
            CreateIndex("dbo.Employees", "Departments_DepartmentId");
            AddForeignKey("dbo.EmployeeDetails", "Department_DepartmentId", "dbo.Departments", "DepartmentId");
            AddForeignKey("dbo.Employees", "Departments_DepartmentId", "dbo.Departments", "DepartmentId");
        }
    }
}
