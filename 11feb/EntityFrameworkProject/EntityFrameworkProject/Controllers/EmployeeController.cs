﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EntityFrameworkProject.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Details()
        {
                EmployeeDBContext emp = new EmployeeDBContext();
                var employee = emp.Employees.ToList();
                return View(employee);
        
        }

        public ActionResult GetDetails()
        {
            EmployeeDetailDBContext emp= new EmployeeDetailDBContext();
            MultiData data = new MultiData();
            data.Employee = emp.EmployeeDetails.ToList();
            data.Department = emp.DepartmentDetails.ToList();
            return View(data);
        }
    }
}