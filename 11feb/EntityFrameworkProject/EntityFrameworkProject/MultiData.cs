﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntityFrameworkProject
{
    public class MultiData
    {
        public IEnumerable<EmployeeDetail> Employee { get; set; }
        public IEnumerable<Departments> Department { get; set; }
    }
}