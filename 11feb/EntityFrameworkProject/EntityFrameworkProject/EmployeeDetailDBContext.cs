﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace EntityFrameworkProject
{
    public class EmployeeDetailDBContext :DbContext
    {
        public DbSet<EmployeeDetail> EmployeeDetails { get; set; }
        public DbSet<Departments> DepartmentDetails { get; set; }
    }
}