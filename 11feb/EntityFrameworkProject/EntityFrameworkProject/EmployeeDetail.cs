﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkProject
{
    public class EmployeeDetail
    {
        [Key]
        public int EmpId { get; set; }
        public string EmployeeName { get; set; }
        public int Salary { get; set; }
        public string Gender { get; set; }
        public Departments Department { get; set; }
    }
}