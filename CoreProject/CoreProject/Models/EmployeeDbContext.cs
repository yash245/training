﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CoreProject.Models
{
    public class EmployeeDbContext : DbContext
    {
        public DbSet<EmployeeDetail> Employees { get; set; }
    }
}
