﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreProject.Models;

namespace CoreProject.Controllers
{
    public class EmployeeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }
        public ActionResult Edit(int id)
        {
            using (EmployeeDbContext entity = new EmployeeDbContext())
            {
                EmployeeDetail employee = entity.Employees.FirstOrDefault(emp => emp.EmployeeId == id);
                return View(employee);
            }
        }
        [HttpPost]
        public IActionResult Edit(EmployeeDetail employee)
        {
            EmployeeDbContext emp = new EmployeeDbContext();
            
            return RedirectToRoute("Index");
        }
        [HttpPost]
        public IActionResult Register(EmployeeDetail  employee)
        {
            if (ModelState.IsValid)
            {
                EmployeeDbContext emp = new EmployeeDbContext();
                emp.Employees.Add(employee);
                return RedirectToRoute("Index");
            }
            else {
                return View();
            }
        }
    }
}