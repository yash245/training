﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Partyproject
{
    class MarriageCeremony : Party
    {
        public int NumPeoples()
        {
            string retakes = null;
            do
            {
                Console.WriteLine("Enter the number of people to get together ");
                string numPeople = Console.ReadLine();
                int numOfPeople;
                if (Int32.TryParse(numPeople, out numOfPeople)) //checking integer or not
                    return numOfPeople;
                else
                {
                    Console.WriteLine("WRONG!!! you entered an invalid choice");
                    Console.WriteLine("Do you want to CONTINUE!! Please your suggestion in the y for Yes ");
                    retakes = Console.ReadLine();
                }

            } while (retakes == "y" || retakes == "Y" || retakes == "yes");
            return 0;
        }
        public override void CalExpenditure(string venueName, float package,string fileName)
        {
            int numOfPeople;
            if (package == 1.6f)
            {
                Party partyObj = new MarriageCeremony();
                partyObj.Venue = venueName;
                partyObj.Catering = 2000;
                partyObj.PakageType = "Silver";
                DateTime d = DateTime.Now;
                partyObj.DateTime = d;
                partyObj.FileName = fileName;
                partyObj.Decoration = 3000;
                numOfPeople = NumPeoples();
                partyObj.NumOfPeople = numOfPeople;
                partyObj.ServiceCharge = package;
                partyObj.display();

            }
            else if (package == 2.4f)
            {
                Party partyObj = new MarriageCeremony();
                partyObj.Venue = venueName;
                partyObj.Catering = 3000;
                partyObj.PakageType = "Gold";
                DateTime d = DateTime.Now;
                partyObj.DateTime = d;
                partyObj.Decoration = 5000;
                partyObj.FileName = fileName;
                numOfPeople = NumPeoples();
                partyObj.NumOfPeople = numOfPeople;
                partyObj.ServiceCharge = package;
                partyObj.display();
            }

            else
            {
                Party partyObj = new MarriageCeremony();
                partyObj.Venue = venueName;
                partyObj.Catering = 5000;
                partyObj.PakageType = "Platinum";
                DateTime d = DateTime.Now;
                partyObj.DateTime = d;
                partyObj.Decoration = 5000;
                partyObj.FileName = fileName;
                numOfPeople = NumPeoples();
                partyObj.NumOfPeople = numOfPeople;
                partyObj.ServiceCharge = package;
                partyObj.display();
            }

        }
        public override void CalPersonExpenditure()
        {

        }
    }
}
