﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Partyproject
{
    public abstract class Party
    {
        public string FileName { get; set; }
        public int Catering { get; set; }
        public int PerPerson { get; set; }
        public int Decoration { get; set; }
        public string Venue { get; set; }
        public DateTime DateTime { get; set; }
        public string PakageType { get; set; }
        public int NumOfPeople { get; set; }
        public float ServiceCharge { get; set; }

        public abstract void CalExpenditure(String venuName, float serviceCharge,string filename);
        public abstract void CalPersonExpenditure();
        public void display()
        {
            StreamWriter fileWriter = File.AppendText(FileName);
            float serCharge = (ServiceCharge * (Catering + Decoration)) * NumOfPeople / 100;
            fileWriter.WriteLine( "*******************Generated Recipte*************************");
            fileWriter.WriteLine("Hotel Name           :" + Venue);
            fileWriter.WriteLine("PackageType          :" + PakageType);
            fileWriter.WriteLine("Date                 :" + DateTime);
            fileWriter.WriteLine("Person               :" + NumOfPeople);
            fileWriter.WriteLine("Cost of a Person     :" + (serCharge + Decoration + Catering) / NumOfPeople);
            fileWriter.WriteLine("Catering             :" + Catering);
            fileWriter.WriteLine("decoration           :" + Decoration);
            fileWriter.WriteLine("ServiceCharge        :" + serCharge);
            fileWriter.WriteLine("--------------------------------------------------------------");
            fileWriter.WriteLine("Grand Total  :" + (serCharge + Decoration + Catering));
            fileWriter.WriteLine("--------------------------------------------------------------");
            fileWriter.Close();
            Console.WriteLine("your file is saved at this place"+ FileName);
        }

    }
}
