﻿using System;

namespace Partyproject
{
    public class Venue
    {
        public void SelectVenue(string filename)
        {
            Console.WriteLine("          \t\t       <<<<<< Welcome to the world of event organiser >>>\n    ");
            Console.WriteLine("\t\t\t<<<<    Is you have to organise an event or a get together plan?? :   >>>>\n");
            Console.WriteLine("\t\t\t<<<<    There are some selected events we have organise               >>>>>\n" + "\t\t\t1.Birthday Party\n" + "\t\t\t2.Marriage ceremony\n" + "\t\t\t3.Anniversery ceremony\n");

            Package packObj = new Package();
            int selVenue;
            string retakes = null;
            Console.WriteLine("\n");
            do
            {
                Console.WriteLine("<<<< There are some selected venue we have organise party as per your requirement>>>>>\n" + "1.Taj Palace\n" + "2.Raddison Blue\n" + "3.The Grand Sobha");
                Console.WriteLine("Please! Enter your input on Numbers Like: 1,2 & 3");

                if (Int32.TryParse(Console.ReadLine(), out selVenue)) //checking integer or not
                {
                    if (selVenue == 1)
                    {
                        packObj.PackageSelector("Taj Palace", filename);
                    }
                    else if (selVenue == 2)
                    {
                        packObj.PackageSelector("Radission blue", filename);
                    }
                    else if (selVenue == 3)
                    {
                        packObj.PackageSelector("The Grand Shobha", filename);
                    }
                    else
                    {
                        Console.WriteLine("WRONG!!! you entered an invalid");
                        Console.WriteLine("Please! Enter a correct input");
                        SelectVenue(filename);
                    }
                }

                else
                {
                    Console.WriteLine("WRONG!!! you entered an invalid choice");
                    Console.WriteLine("Do you want to CONTINUE!! Please your suggestion in the y for Yes ");
                    retakes = Console.ReadLine();
                }

            } while (retakes == "y" || retakes == "Y" || retakes == "yes");
        }
    }
}
