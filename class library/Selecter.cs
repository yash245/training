﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Partyproject
{
    class Selecter
    {
        public void SelectEvent(string venuePrice, float packageprice,string filename)
        {

            string retakes = null;
            Console.WriteLine("<<<< There are some selected events we have organise>>>>>\n" + "1.Birthday Party\n" + "2.Marriage ceremony\n" + "3.Anniversery ceremony");
            Console.WriteLine("Enter your input in the numbers 1,2 & 3");
            int selEvent;
            Console.WriteLine("\n");
            do
            {
                if (Int32.TryParse(Console.ReadLine(), out selEvent)) //checking integer or not
                {
                    string retake = null;

                    if (selEvent == 1)
                    {
                        Birthday birthObj = new Birthday();
                        birthObj.CalExpenditure(venuePrice, packageprice, filename);
                        birthObj.CalPersonExpenditure();
                    }
                    else if (selEvent == 3)
                    {
                        AnniversaryCeremony annObj = new AnniversaryCeremony();
                        annObj.CalExpenditure(venuePrice, packageprice, filename);
                        annObj.CalPersonExpenditure();
                    }
                    else if (selEvent == 2)
                    {
                        MarriageCeremony marriObj = new MarriageCeremony();
                        marriObj.CalExpenditure(venuePrice, packageprice, filename);
                        marriObj.CalPersonExpenditure();
                    }
                    else
                    {
                        Console.WriteLine("WRONG!!! you entered an invalid");
                        Console.WriteLine("Please! Enter a correct input");
                        SelectEvent(venuePrice, packageprice,filename);
                    }
                }

                else
                {
                    Console.WriteLine("WRONG!!! you entered an invalid choice");
                    Console.WriteLine("Do you want to CONTINUE!! Please your suggestion in the y for Yes ");
                    retakes = Console.ReadLine();
                }

            } while (retakes == "y" || retakes == "Y" || retakes == "yes");
        }

    }
}
