﻿using Microsoft.EntityFrameworkCore;
using System;

namespace DataLayer
{
    public class DataContext : DbContext
    {
        private string _connectionString;
        public DataContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }
        public virtual DbSet<Department> Departments { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer(_connectionString);

            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Department>(e =>
            {
                e.ToTable("Dpartments");

                e.HasKey(x => x.Id);

            });

        }


    }
}
