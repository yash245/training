﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer
{
    public class Department
    {

        public string DepartmentName { get; set; }
        public int Id { get; set; }
    }
}
